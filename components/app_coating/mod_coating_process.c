/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		mod_coating_process.c
* @date		20 jul. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "stdio.h"
#include "stddef.h"

#include "app_coating.h"
#include "mod_coating_process.h"
#include "mod_coating_handlers.h"



#include "api.h"
#include "board_timer.h"

#include "esp_log.h"

#include "machine.h"
#include "os_config.h"

/********************** macros and definitions *******************************/
#define MAX_ESTATIC_COMMAND 	8

/********************** internal data declaration ****************************/
/*CmdProcessCustom, the arguments can be modified to make a load and run after*/
//processCommand_t cmdProcessCustom[MAX_ESTATIC_COMMAND] = {
//
//		/*Al setear el custom program arrancamos desde el comando numero 0*/
//
//		{ .commandnumber = PROCESS_COMMAND_DOWN, 		.argument.spin.velocity = 400000,	.argument.spin.acceleration = 40000 , 	.fpcommandhandler = HandlerDown },
//		{ .commandnumber = PROCESS_COMMAND_WAIT, 		.argument.wait.time = 0,							 .fpcommandhandler = HandlerWait},
//		/*start of the DWUW cycle*/
//		{ .commandnumber = PROCESS_COMMAND_DOWN, 		.argument.spin.velocity = 400000,	.argument.spin.acceleration = 40000 , 	.fpcommandhandler = HandlerDownLoop },
//		{ .commandnumber = PROCESS_COMMAND_WAIT, 		.argument.wait.time = 0,							.fpcommandhandler = HandlerWaitDown },
//		{ .commandnumber = PROCESS_COMMAND_UP, 			.argument.spin.velocity = 400000,	.argument.spin.acceleration = 40000 , 	.fpcommandhandler = HandlerUpLoop },
//		{ .commandnumber = PROCESS_COMMAND_WAIT, 		.argument.wait.time = 0,							.fpcommandhandler = HandlerWaitUp },
//		/*LOOP repeats N times the DWUW cycle */
//		{ .commandnumber =  PROCESS_COMMAND_LOOP, 		.argument.value.val = 0 },
//		{ .commandnumber = PROCESS_COMMAND_FINISH,		.argument.spin.velocity = 0,	.argument.spin.acceleration = 0, .fpcommandhandler = HandlerFinish},
//
//};



processCommand_t cmdProcessCustom[MAX_ESTATIC_COMMAND] = {

/*Al setear el custom program arrancamos desde el comando numero 0*/

{ .commandnumber = PROCESS_COMMAND_DOWN, 		.argument.spin.velocity = 50000,	.argument.spin.acceleration = 65000 , 	.fpcommandhandler = HandlerDownUntil},
{ .commandnumber = PROCESS_COMMAND_WAIT, 		.argument.wait.time = 1,							 					 	.fpcommandhandler = HandlerWait},
/*start of the DWUW cycle*/
{ .commandnumber = PROCESS_COMMAND_DOWN, 		.argument.spin.velocity = 100000,	.argument.spin.acceleration = 65000 , 	.fpcommandhandler = HandlerDownLoop },
{ .commandnumber = PROCESS_COMMAND_WAIT, 		.argument.wait.time = 1,													.fpcommandhandler = HandlerWaitDown },
{ .commandnumber = PROCESS_COMMAND_UP, 			.argument.spin.velocity = 100000,	.argument.spin.acceleration = 65000 , 	.fpcommandhandler = HandlerUpLoop },
{ .commandnumber = PROCESS_COMMAND_WAIT, 		.argument.wait.time = 1,													.fpcommandhandler = HandlerWaitUp },
/*LOOP repeats N times the DWUW cycle */
{ .commandnumber =  PROCESS_COMMAND_LOOP, 		.argument.value.val = 3 },
{ .commandnumber = PROCESS_COMMAND_FINISH,		.argument.spin.velocity = 0,	.argument.spin.acceleration = 0, 					.fpcommandhandler = HandlerFinish},

};


process_t processDipCoating;

/********************** internal data definition *****************************/

static const char* TAG = "mod_coating_process ";

/********************** external data definition *****************************/

/********************** internal functions definition ************************/


static  float mod_coating_process_convert_speed_mm_min_to_ustep_s_(float vel ){
	return vel*MACHINE_USTEPS_VELOCITY;
}
static float  mod_coating_process_convert_accel_m_min2_to_usteps_s2_(float accel){

	//ESP_LOGI(TAG,"procces_accel:%f",MACHINE_USTEPS_ACCELERATION);
	return accel*MACHINE_USTEPS_ACCELERATION;
}
static float  mod_coating_process_convert_distance_mm_to_usteps_(float dist){
	return dist*MACHINE_STEPS_PER_MILLIMETER;
}

/********************** external functions definition ************************/


//PROCESS RUN FUNCTION

void mod_coating_process_run(process_t *process) {

	uint8_t command_index;
	uint8_t loop_ori,loop;

	process->state.commandActualIndex = 0;

	processDipCoating.ms_last_start_time=board_timer_get_time_ms();

	if (process->command != NULL) {
		command_index = process->state.commandIndex; // command index
		loop_ori = process->command[PROCESS_COMMAND_LOOP-2].argument.value.val;
		loop=loop_ori;

		ESP_LOGI(TAG,"Proceso Iniciado - Motor Encendido");

		if (processDipCoating.config.status){
			while ( (process->state.commandActualIndex < command_index) && true == processDipCoating.config.status ) {
				if (process->command[process->state.commandActualIndex].commandnumber != PROCESS_COMMAND_LOOP) {
						//VER LO QUE ESTAMOS PONIENDO EN EL PUNTERO A FUNCION (PASAR DIRECTAMENTE EL ARGUMENTO )
						//process->command[index].fpcommandhandler(&(process->command[index].argument.spin));
						process->command[process->state.commandActualIndex].fpcommandhandler(&(process->command[process->state.commandActualIndex].argument));
						//index++;
						process->state.commandActualIndex++;
				}
				if (process->command[process->state.commandActualIndex].commandnumber == PROCESS_COMMAND_LOOP){
						if (loop > 0 ){
							loop--;
							ESP_LOGI(TAG,"Loop number:%d",loop_ori - loop);
							//Desde comando LOOP vuelve 4 comandos hacia atras para hacer secuencia Down-Wait-Up-Wait
							process->state.commandActualIndex-=4;
						}
						//else index++;
						else process->state.commandActualIndex++;
				}
			}
		}
	} else  ESP_LOGI(TAG,"Without program to execute!");
}


//SINGLE MOVEMENT COMMAND FUNCTIONS

void mod_coating_process_cero_machine(){

	processCommandArgSpin_t parameters;


	parameters.velocity=mod_coating_process_convert_speed_mm_min_to_ustep_s_(MACHINE_MANUAL_VELOCITY);
	parameters.acceleration=mod_coating_process_convert_accel_m_min2_to_usteps_s2_(MACHINE_MANUAL_MEDIUM_ACCELERATION);



	HandlerCeroMachine(&parameters);


}



//SINGLE MOVEMENT COMMAND FUNCTIONS

/*++++++++++++++++++++++++++++++++++++FAST+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void mod_coating_process_command_up_fast_(){

	processCommandArgSpin_t parameters;

	parameters.velocity=mod_coating_process_convert_speed_mm_min_to_ustep_s_(MACHINE_MANUAL_VELOCITY);
	parameters.acceleration=(uint32_t) mod_coating_process_convert_accel_m_min2_to_usteps_s2_(MACHINE_MANUAL_MEDIUM_ACCELERATION);
	parameters.displacement_z=mod_coating_process_convert_distance_mm_to_usteps_(MACHINE_MANUAL_DISPLACEMENT_50);

	HandlerUp_without_program(&parameters);
}

void mod_coating_process_command_up_(){

	processCommandArgSpin_t parameters;

	parameters.velocity=mod_coating_process_convert_speed_mm_min_to_ustep_s_(MACHINE_MANUAL_VELOCITY);
	parameters.acceleration=(uint32_t) mod_coating_process_convert_accel_m_min2_to_usteps_s2_(MACHINE_MANUAL_MEDIUM_ACCELERATION);
	parameters.displacement_z=mod_coating_process_convert_distance_mm_to_usteps_(MACHINE_MANUAL_DISPLACEMENT_10);

	HandlerUp_without_program(&parameters);
}

void mod_coating_process_command_up_slow_(){

	processCommandArgSpin_t parameters;

	parameters.velocity=mod_coating_process_convert_speed_mm_min_to_ustep_s_(MACHINE_MANUAL_VELOCITY);
	parameters.acceleration=(uint32_t) mod_coating_process_convert_accel_m_min2_to_usteps_s2_(MACHINE_MANUAL_MEDIUM_ACCELERATION);
	parameters.displacement_z=mod_coating_process_convert_distance_mm_to_usteps_(MACHINE_MANUAL_DISPLACEMENT_1);

	HandlerUp_without_program(&parameters);
}

/*+++++++++++++++++++++++++++++++++++DOWN++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void mod_coating_process_command_down_test_ () {

	processCommandArgSpin_t parameters;

	/*Test Desplazamiento*/

	uint32_t velocidades[10] = { 1, 10,25,50, 100, 200, 500, 800, 1000,2000 };
	uint32_t acelerariones[4] = { 100,500, 1000, 2100 };

	int i, j;

	//desplazamiento
	parameters.displacement_z = mod_coating_process_convert_distance_mm_to_usteps_ (50);

//vel  0 - 2'23 -512
//    1  mm/min       226.12
//    10  mm/min			2261.2
//    100  mm/min			22612
//    200  mm/min			45224
//    500  mm/min			113060
//    800  mm/min			180896
//    1000  mm/min    226120
//	2000  mm/min    	452240
//	5000  mm/min			1130600
//	10000  mm/min			2261200

//Accel   0  65000     2'16-1

// 10   m/min2       308.187
// 100  m/min2       3081.87
// 500  m/min2       15409.35
// 800  m/min2       24654.96
// 1000 m/min2       30818.7
// 1200 m/min2       36982.44
// 1500 m/min2			 46228.05
// 2100 m/min2			 64719.4
//

	for (i = 4; i < 10 ; i++) {
		for (j = 0; j < 4; j++) {
			parameters.velocity = mod_coating_process_convert_speed_mm_min_to_ustep_s_ (velocidades[i]);
			parameters.acceleration = (uint32_t) mod_coating_process_convert_accel_m_min2_to_usteps_s2_ (acelerariones[j]);

			HandlerDown_without_program (&parameters);
			HandlerUp_without_program (&parameters);

		}
	}

}

void mod_coating_process_command_down_fast_(){

	processCommandArgSpin_t parameters;

	parameters.velocity=mod_coating_process_convert_speed_mm_min_to_ustep_s_(MACHINE_MANUAL_VELOCITY);
	parameters.acceleration=(uint32_t) mod_coating_process_convert_accel_m_min2_to_usteps_s2_(MACHINE_MANUAL_MEDIUM_ACCELERATION);
	parameters.displacement_z=mod_coating_process_convert_distance_mm_to_usteps_(MACHINE_MANUAL_DISPLACEMENT_50);

	HandlerDown_without_program(&parameters);
}

void mod_coating_process_command_down_(){

#if 1

	processCommandArgSpin_t parameters;
	parameters.velocity=mod_coating_process_convert_speed_mm_min_to_ustep_s_(MACHINE_MANUAL_VELOCITY);
	parameters.acceleration=(uint32_t) mod_coating_process_convert_accel_m_min2_to_usteps_s2_(MACHINE_MANUAL_MEDIUM_ACCELERATION);
	parameters.displacement_z=mod_coating_process_convert_distance_mm_to_usteps_(MACHINE_MANUAL_DISPLACEMENT_10);

	HandlerDown_without_program(&parameters);
#endif

/*Funcio para pruebas de dezplazamiento*/
#if 0
	mod_coating_process_command_down_test_();
#endif

}

void mod_coating_process_command_down_slow_(){

	processCommandArgSpin_t parameters;

	parameters.velocity=mod_coating_process_convert_speed_mm_min_to_ustep_s_(MACHINE_MANUAL_VELOCITY);
	parameters.acceleration=(uint32_t) mod_coating_process_convert_accel_m_min2_to_usteps_s2_(MACHINE_MANUAL_MEDIUM_ACCELERATION);
	parameters.displacement_z=mod_coating_process_convert_distance_mm_to_usteps_(MACHINE_MANUAL_DISPLACEMENT_1);

	HandlerDown_without_program(&parameters);
}



/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void mod_coating_process_command_stop(){

	processCommandArgSpin_t parameters;

	parameters.velocity=0;
	parameters.acceleration=0;
	parameters.displacement_z=0;

	HandlerStop(&parameters);
}

void mod_coating_process_command_read_data_(){
	HandlerREADDATA();
}



void mod_coating_process_command_ena_driver_(){
	HandlerENA_DRIVER();
}

void mod_coating_process_command_dis_driver_(){
	HandlerDIS_DRIVER();
}



/* Parametros de configuracion de proceso */

/*Ubicacion del cero de la muestra*/
void mod_coating_process_command_cero_sample_(){
	HandlerCERO_SAMPLE();
}


/*Profundidad de dip coating */
void mod_coating_process_command_delta_dip_(){
	HandlerDELTADIP();
}


void mod_coating_process_command_run_(){
	HandlerRun();
}

void mod_coating_process_command_reset_(){
	HandlerRESET();
}

void mod_coating_process_init(void)
{

	/*Initial DipCoater Config*/

	/*Charge Custom process*/
	processDipCoating.command= cmdProcessCustom;

	processDipCoating.commandlen=0;
	processDipCoating.state.commandIndex=MAX_ESTATIC_COMMAND;
	processDipCoating.state.commandActualIndex=0;
	processDipCoating.state.flags=0;

	processDipCoating.config.status=0;

	processDipCoating.config.new_botton_limit= MACHINE_CONTROL_MECANICAL_LOWER_LIMIT;
	processDipCoating.run_protocol =false;


	/*Start system search zero machine*/
	processDipCoating.config.cero_machine=true;

}
void mod_coating_process_deinit(void){}

//Se encarga de procesar la cola de comandos recibidos,  (Puede recibir comandos individuales o un programa completo)
void mod_coating_process_loop (void) {

	processCommand_t readed_Command;
	api_program_manager_program_t aux_programDipCoating;

	/*Search Zero Machine*/
	if (true == processDipCoating.config.cero_machine) {
		mod_coating_process_cero_machine();
		api_stone_open_window("menu");
	}


	/*Run Complete Program*/
	if (true == processDipCoating.run_protocol) {

		processDipCoating.config.status = 1;
		mod_coating_process_run(&processDipCoating);
		processDipCoating.config.status = 0;
		processDipCoating.run_protocol = false;
	}

	/*Receive Complete program*/
	if (pdPASS == xQueueReceive (xQueueIndividualCommand, &readed_Command, (TickType_t) 0)) {

		if (0 == processDipCoating.config.status) {

			switch (readed_Command.commandnumber) {
				case PROCESS_COMMAND_CERO_MACHINE:
					mod_coating_process_cero_machine ();
					break;
				case PROCESS_COMMAND_DOWNFAST:
					mod_coating_process_command_down_fast_();
					break;
				case PROCESS_COMMAND_DOWNSLOW:
					mod_coating_process_command_down_slow_();
					break;
				case PROCESS_COMMAND_DOWN:
					mod_coating_process_command_down_();
					break;
				case PROCESS_COMMAND_UPFAST:
					mod_coating_process_command_up_fast_();
					break;
				case PROCESS_COMMAND_UPSLOW:
					mod_coating_process_command_up_slow_();
					break;
				case PROCESS_COMMAND_UP:
					mod_coating_process_command_up_();
					break;
				case PROCESS_COMMAND_RUN:
					mod_coating_process_command_run_();
					break;
				case PROCESS_COMMAND_READDATA:
					mod_coating_process_command_read_data_();
					break;
				case PROCESS_COMMAND_ENA_DRIVER:
					mod_coating_process_command_ena_driver_();
					break;
				case PROCESS_COMMAND_DIS_DRIVER:
					mod_coating_process_command_dis_driver_();
					break;
				case PROCESS_COMMAND_DELTADIP:
					mod_coating_process_command_delta_dip_();
					break;
				case PROCESS_COMMAND_CERO_SAMPLE:
					mod_coating_process_command_cero_sample_();
					break;
				case PROCESS_COMMAND_RESET:
					mod_coating_process_command_reset_();
					break;
				default:
					break;
			}
		}
		else 	{
			ESP_LOGE(TAG, "Command deleted, machine working!");
		}

	}

		/*Receive Complete program*/

		if ( pdPASS == xQueueReceive (xQueueCompleteProgram, &aux_programDipCoating, (TickType_t) 0) ) {


			if (0 == processDipCoating.config.status) {
			/*SETCUSTOMPROGRAMAPP  down_vel down_acel down_wait  up_vel up_acel up_wait total_loop   */

			strcpy(processDipCoating.last_program_name,	aux_programDipCoating.name);
			// DOWN LOOP vel acel
			processDipCoating.command[2].argument.spin.velocity =  mod_coating_process_convert_speed_mm_min_to_ustep_s_(aux_programDipCoating.velocity_down);
			processDipCoating.command[2].argument.spin.acceleration = mod_coating_process_convert_accel_m_min2_to_usteps_s2_(aux_programDipCoating.acceleration_down); // 1 - 64500
			//WAIT DOWN
			processDipCoating.command[3].argument.wait.time = aux_programDipCoating.wait_down;
			//UP LOOP vel acel
			processDipCoating.command[4].argument.spin.velocity = mod_coating_process_convert_speed_mm_min_to_ustep_s_(aux_programDipCoating.velocity_up);
			processDipCoating.command[4].argument.spin.acceleration = mod_coating_process_convert_accel_m_min2_to_usteps_s2_(aux_programDipCoating.acceleration_up); // 1 - 64500
			//WAIT UP
			processDipCoating.command[5].argument.wait.time = aux_programDipCoating.wait_up;
			//NUMBER OF LOOP
			processDipCoating.command[6].argument.spin.velocity = aux_programDipCoating.loops - 1;
			//Desplazamiento hasta muestra
			processDipCoating.config.displacement_to_sample = mod_coating_process_convert_distance_mm_to_usteps_(aux_programDipCoating.displacement_to_sample); // Expresado en mm
			//Profundidad DELTADIP
			processDipCoating.config.displacement_delta_sample = mod_coating_process_convert_distance_mm_to_usteps_(aux_programDipCoating.depth_sample); // Expresado en mm
		}

		else 	{
			ESP_LOGE(TAG, "Command deleted, machine working!");
			}
	}


}

/********************** end of file ******************************************/

/** @}Final Doxygen */
