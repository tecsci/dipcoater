/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		mod_coating_emergency.c
* @date		20 jul. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "stdio.h"
#include "stddef.h"

#include "app_coating.h"
#include "mod_coating_process.h"
#include "mod_coating_handlers.h"
#include "mod_coating_emergency.h"

#include "api.h"
#include "esp_log.h"
#include "os_config.h"

/********************** macros and definitions *******************************/


/********************** internal data declaration ****************************/


/********************** internal data definition *****************************/

static const char* TAG= "mod_coating_emergency ";

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/


void mod_coating_emergency_init(void){

	xTaskCreate(mod_coating_emergency_loop, "mod coating  emergency task" 	// A name just for humans
			, OS_CONFIG_MOD_COATING_EMERGENCY_TASK_SIZE 			// This stack size can be checked & adjusted by reading the Stack Highwater
			, NULL, OS_CONFIG_MOD_COATING_EMERGENCY_TASK_PRIORITY 		// Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
			, NULL);
}
void mod_coating_emergency_deinit(void){}
void mod_coating_emergency_loop (void) {
	processCommand_t readed_Command;

	for (;;) {

		//Receive individual Commads
		if (xQueueReceive (xQueueIndividualCommand_Stop, &readed_Command, portMAX_DELAY)) {

			if (PROCESS_COMMAND_STOP == readed_Command.commandnumber) {
				mod_coating_process_command_stop();
			}
		}
	}
}

/********************** end of file ******************************************/

/** @}Final Doxygen */
