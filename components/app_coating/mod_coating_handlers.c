/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		mod_coating_handlers.c
* @date		20 jul. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "stdio.h"
#include "stddef.h"
#include "esp_system.h"
#include "esp_log.h"

#include "app_coating.h"
#include "mod_coating_handlers.h"
#include "machine.h"
#include "board.h"
#include "os_config.h"
#include "api.h"

#include "TMC_Board.h"
#include "TMC5130_Register.h"

/********************** macros and definitions *******************************/
static const char* TAG = "mod_coating_handlers";

/********************** internal data declaration ****************************/
/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/**Registros


Evalboards.ch1.writeRegister(0, 0x26,  arg->acceleration);
Evalboards.ch1.writeRegister(0, 0x27, arg->velocity);
Evalboards.ch1.writeRegister(0, 0x28,  arg->desaccleration);
Enable sg_stop  by stallGuard2
Evalboards.ch1.writeRegister(0, 0x34, 0x00000400);

Detection Flag Stallguard    Mascara(0x0000040)
Evalboards.ch1.readRegister(0, TMC5130_RAMPSTAT, &lectura);      TMC5130_RAMPSTAT  0x35
if (lectura & 0x0000040)

Detection TMC5130_RAMPSTAT flag XACTUAL == XTARGET
Evalboards.ch1.readRegister(0, TMC5130_RAMPSTAT, &lectura);
if (lectura & 0x00000200)

Read Actual Position
Evalboards.ch1.readRegister(0, 0x21, &lectura);

Target Position   lo desplaza 0xFF...
Evalboards.ch1.writeRegister(0, 0x2D, 0xFFB1E000);

*/


/*Control del limite mecanico (recorrido) de la maquina, los valores aqui dependen del largo del eje Z*/
static int32_t mod_coating_handlers_control_limit_(int32_t);

/********************** external functions definition ************************/
void mod_coating_handlers_control_change_bottom_limit(float  value){

	processDipCoating.config.new_botton_limit= value * MACHINE_STEPS_PER_MILLIMETER;

}

float  mod_coating_handlers_control_get_bottom_limit(void){
	return ((float) processDipCoating.config.new_botton_limit / (float)MACHINE_STEPS_PER_MILLIMETER);
}


/*Handler Functions*/
int HandlerCeroMachine(processCommandArgSpin_t *arg) {

	int32_t lectura;
	static uint8_t stallguard_flag=0;

	ESP_LOGI(TAG,"%s",__func__);
	ESP_LOGI(TAG,"Velocity:%d",arg->velocity);
	ESP_LOGI(TAG,"Acceleration:%d",arg->acceleration);


	Evalboards.ch1.enableDriver(DRIVER_ENABLE);
	processDipCoating.config.status=1;

	//set velocidad
	//V1
	Evalboards.ch1.writeRegister(0, TMC5130_V1, (arg->velocity) / 2);
	//VMAX
	Evalboards.ch1.writeRegister(0, TMC5130_VMAX, arg->velocity);
	//VSTART
	Evalboards.ch1.writeRegister(0, TMC5130_VSTART, 0);
	//VSTOT
	Evalboards.ch1.writeRegister(0, TMC5130_VSTOP, 100);

	//setaceleracion
	//A1
	Evalboards.ch1.writeRegister(0, TMC5130_A1,  arg->acceleration);
	//AMAX
	Evalboards.ch1.writeRegister(0, TMC5130_AMAX,  arg->acceleration);

	//set desaceleracion

	//DMAX
	Evalboards.ch1.writeRegister(0, TMC5130_DMAX,  arg->acceleration);
	//D1
	Evalboards.ch1.writeRegister(0, TMC5130_D1,  arg->acceleration);

	/*Setting Mode RampMode   (Position Mode(using A D and V parameters))*/
	Evalboards.ch1.writeRegister(0, TMC5130_RAMPMODE, 0x00000000);

	/*Enable sg_stop  by stallGuard2*/
	Evalboards.ch1.writeRegister(0, TMC5130_SWMODE, 0x00000400);

	/*Write Taget Position * Lo desplazo hacia arriba para buscar el cero machine* */
	//Evalboards.ch1.writeRegister(0, TMC5130_XTARGET, 0xFFB1E000);   // Decimal con signo: -5120000
	Evalboards.ch1.writeRegister(0, TMC5130_XTARGET, MACHINE_CONTROL_MECANICAL_LOWER_LIMIT_STALLGUARD_DETECT);   // Decimal con signo: -5120000


	//Detecto flag de stallguard
	while (1 == processDipCoating.config.status ) {

		Evalboards.ch1.readRegister(0, TMC5130_RAMPSTAT, &lectura);


		//if(true == api_tmc_was_stallguard_detected(lectura)) {
		if (lectura & 0x0000040) {

			Evalboards.ch1.writeRegister(0, TMC5130_RAMPMODE,0x00000000);
			Evalboards.ch1.writeRegister(0, TMC5130_XACTUAL, 0x00000000);
			Evalboards.ch1.writeRegister(0, TMC5130_XTARGET, MACHINE_CONTROL_MECANICAL_UPPER_LIMIT);

			Evalboards.ch1.readRegister(0, TMC5130_RAMPSTAT, &lectura);

			Evalboards.ch1.writeRegister(0, TMC5130_SWMODE, 0x00000000);
			//Evalboards.ch1.writeRegister(0, 0x34, 0x00000400);
			//Evalboards.ch1.writeRegister(0, 0x34, 0x00000000);

			stallguard_flag=1;

			break;
		}
		else{
			vTaskDelay(OS_CONFIG_MOD_HANDLERS_COMMANDS_TASK_PERIOD / portTICK_PERIOD_MS);
		}

	}

	/*Una vez detectado el cero realizo un movimiento fijo con 127372 (10 mm) pasos hacia abajo, detecto el flag que detecta XACTUAL=XTARGET*/
	while ( (1 == stallguard_flag)  &&  (1 == processDipCoating.config.status) ) {

		/*Detecto TMC5130_RAMPSTAT flag XACTUAL == XTARGET*/
		Evalboards.ch1.readRegister(0, TMC5130_RAMPSTAT, &lectura);

		//if (true == api_tmc_is_position_target(lectura)){
		if (lectura & 0x00000200){
			stallguard_flag=0;
			break;
		}
		else {
			vTaskDelay(OS_CONFIG_MOD_HANDLERS_COMMANDS_TASK_PERIOD / portTICK_PERIOD_MS);
		}

	}

	Evalboards.ch1.enableDriver(DRIVER_DISABLE);

	Evalboards.ch1.readRegister(0, TMC5130_XTARGET, &lectura);
	ESP_LOGI(TAG,"X_TARGET:%d",lectura);

	Evalboards.ch1.readRegister(0, TMC5130_XACTUAL, &lectura);
	ESP_LOGI(TAG,"A_TARGET:%d",lectura);

	processDipCoating.config.status=0;
	processDipCoating.config.cero_machine=false;

	return 0;

}

int HandlerSpin(processCommandArgSpin_t*	arg) {

	ESP_LOGI(TAG,"%s",__func__);


	ESP_LOGI(TAG,"velocidad:%d",arg->velocity);
	ESP_LOGI(TAG,"aceleracion:%d",arg->acceleration);
	vTaskDelay(2000 / portTICK_PERIOD_MS);
	return 0;
}



/*********************************************************UP*********************************************************************************************************/

//int HandlerUp(processCommandArgSpin_t*	arg) {
//
//	int32_t lectura;
//
//	printf("\r\n");
//	printf("Subiendo muestra!!\t");
//	printf("velocidad:%d\t",arg->velocity);
//	printf("aceleracion:%d\t",arg->acceleration);
//
//
//	Evalboards.ch1.readRegister(0, 0x2D, &lectura);
//	printf("Posicion ATARGET :%d\r\n", lectura);
//
//
//	// Seteo de registro XTARGET
//	Evalboards.ch1.writeRegister(0, 0x2D, lectura - arg->displacement_z );
//
//	// Seteo aceleracion y velocidad
//	Evalboards.ch1.writeRegister(0, 0x26,  arg->acceleration);
//	Evalboards.ch1.writeRegister(0, 0x27, arg->velocity);
//
//	// Seteo desaceleracion
//	Evalboards.ch1.writeRegister(0, 0x28,  arg->acceleration);
//
//	//Una vez detectado el cero realizo un movimiento fijo con 512000 pasos hacia abajo, detecto el flag que detecta XACTUAL=XTARGET
//	while (processDipCoating.config.status) {
//
//		Evalboards.ch1.readRegister(0, TMC5130_RAMPSTAT, &lectura);
//		if (lectura & 0x00000200)
//			break;
//		else
//		  vTaskDelay(100 / portTICK_PERIOD_MS);
//
//	}
//
//	Evalboards.ch1.readRegister(0, 0x2D, &lectura);
//	printf("Posicion ATARGET :%d\r\n", lectura);
//
//	return 0;
//}

//Subida dentro del proceso de ejecucion de programas

int HandlerUpLoop(processCommandArgSpin_t*	arg) {
	int32_t lectura;

	ESP_LOGI(TAG,"%s",__func__);
	ESP_LOGI(TAG,"Vel:%d",arg->velocity);
	ESP_LOGI(TAG,"Accel:%d",arg->acceleration);


	//Leo Actual

	Evalboards.ch1.readRegister(0, TMC5130_XACTUAL, &lectura);

	// Seteo aceleracion y velocidad
	Evalboards.ch1.writeRegister(0, TMC5130_AMAX,  arg->acceleration);
	Evalboards.ch1.writeRegister(0, TMC5130_VMAX, arg->velocity);

	//desaceleracion
	Evalboards.ch1.writeRegister(0, TMC5130_DMAX,  arg->acceleration);


	// Seteo de registro XTARGET
	Evalboards.ch1.writeRegister(0, TMC5130_XTARGET, lectura - processDipCoating.config.displacement_delta_sample  );


	//Una vez detectado el cero realizo un movimiento fijo con 512000 pasos hacia abajo, detecto el flag que detecta XACTUAL=XTARGET
	while (processDipCoating.config.status) {

		Evalboards.ch1.readRegister(0, TMC5130_RAMPSTAT, &lectura);
		if (true == api_tmc_is_position_target(lectura))
			break;
		else if(true == api_tmc_was_vzero_detected(lectura))
			break;
		else
		  vTaskDelay(OS_CONFIG_MOD_HANDLERS_COMMANDS_TASK_PERIOD / portTICK_PERIOD_MS);
	}
	return 0;
}


int HandlerUp_without_program(processCommandArgSpin_t*	arg) {


	int32_t reg_rampstat, position_actual , position_target;
#if 0 //FOR TESTING
	int64_t  initial_time, final_time =0, delta_time,t_s1=0,t_s2=0;
#endif
	int32_t stage=1;
	//ESP_LOGI(TAG,"%s",__func__);

	processDipCoating.config.status=1;
	Evalboards.ch1.enableDriver(DRIVER_ENABLE);

	//Leo Actual
	Evalboards.ch1.readRegister(0, TMC5130_XACTUAL, &position_actual);

	// velocidad
	//V1
	Evalboards.ch1.writeRegister(0, TMC5130_V1, (arg->velocity) / 2);
	//VMAX
	Evalboards.ch1.writeRegister(0, TMC5130_VMAX, arg->velocity);
	//VSTART
	Evalboards.ch1.writeRegister(0, TMC5130_VSTART, 0);
	//VSTOT
	Evalboards.ch1.writeRegister(0, TMC5130_VSTOP, 100);

	// Seteo aceleracion
	//A1
	Evalboards.ch1.writeRegister(0, TMC5130_A1,  arg->acceleration);
	//AMAX
	Evalboards.ch1.writeRegister(0, TMC5130_AMAX,  arg->acceleration);

	// Seteo desaceleracion
	//DMAX
	Evalboards.ch1.writeRegister(0, TMC5130_DMAX,  arg->acceleration);
	//D1
	Evalboards.ch1.writeRegister(0, TMC5130_D1,  arg->acceleration);

	// Seteo de registro XTARGET
	position_target=position_actual - arg->displacement_z;
	position_target = mod_coating_handlers_control_limit_(position_target);

#if 0
	initial_time = board_timer_get_time_micros();
#endif

	Evalboards.ch1.writeRegister(0, TMC5130_XTARGET,position_target  );

	// Detecto el flag que detecta XACTUAL=XTARGET y apago driver
	while (1 == processDipCoating.config.status) {
		Evalboards.ch1.readRegister(0, TMC5130_RAMPSTAT, &reg_rampstat);
		//Evalboards.ch1.readRegister(0, 0x21, &position_actual);

#if 0 //FOR TESTING
		if(1 == stage){
			if ( (reg_rampstat & 0x00000100)){
				t_s1 = board_timer_get_time_micros();
				stage = 2;
			}
		}

		if(2 == stage){
			if ( !(reg_rampstat & 0x00000100)){
				t_s2 = board_timer_get_time_micros();
				stage = 3;
			}
		}
#endif


		/*Leo registro y comparo, velocidad zero  y  position_actual == position_target */
		//if ( reg_rampstat & 0x00000600){
		if ( (true == api_tmc_is_position_target(reg_rampstat)) || (true == api_tmc_was_vzero_detected(reg_rampstat)) ){

#if 0 //FOR TESTING
			//final_time= board_timer_get_time_ms();
			final_time = board_timer_get_time_micros();

			delta_time=final_time  - initial_time;
			//ESP_LOGI(TAG," Vel:%d  Acel:%d Time Down:%d  inicial:%lld  ts1:%d  ts2: %d final:%lld ",arg->velocity, arg->acceleration,  (int32_t)delta_time, initial_time,t_s1,t_s2,final_time);
			//printf("dip_exp_2 %d,%d,%d,%d,%lld,%lld,%lld \n",1,arg->velocity, arg->acceleration,arg->displacement_z,delta_time,t_s1-initial_time,final_time-t_s2);
			//#dip_exp_2 id,velocidad,aceleracion,desplazamiento,delta_total,delta_acc,delta_dec \n
			//printf("start:%lld stop:%lld delta:%lld t_s1:%lld ts_2:%lld\r\n",initial_time, final_time, delta_time,t_s1,t_s2);
#endif

			break;
		}
		else{

			vTaskDelay(OS_CONFIG_MOD_HANDLERS_COMMANDS_TASK_PERIOD / portTICK_PERIOD_MS);
		}
	}

	vTaskDelay(OS_CONFIG_MOD_HANDLERS_COMMANDS_TASK_PERIOD_LONG / portTICK_PERIOD_MS);
	processDipCoating.config.status=0;
	Evalboards.ch1.enableDriver(DRIVER_DISABLE);


	Evalboards.ch1.readRegister(0, 0x21, &position_actual);
	Evalboards.ch1.readRegister(0, 0x2D, &position_target);
	//ESP_LOGI(TAG,"Position->  Actual:%d		Target:%d",position_actual,position_target);



	return 0;
}


/*********************************************************DOWN*********************************************************************************************************/


int HandlerDownUntil(processCommandArgSpin_t*	arg) {
	int32_t lectura, position_target;

	ESP_LOGI(TAG,"%s",__func__);

	ESP_LOGI(TAG,"Vel:%d",arg->velocity);
	ESP_LOGI(TAG,"Accel:%d",arg->acceleration);

	// velocidad
	//V1
	Evalboards.ch1.writeRegister(0, TMC5130_V1, (arg->velocity) / 2);
	//VMAX
	Evalboards.ch1.writeRegister(0, TMC5130_VMAX, arg->velocity);
	//VSTART
	Evalboards.ch1.writeRegister(0, TMC5130_VSTART, 0);
	//VSTOT
	Evalboards.ch1.writeRegister(0, TMC5130_VSTOP, 100);

	// Seteo aceleracion
	//A1
	Evalboards.ch1.writeRegister(0, TMC5130_A1,  arg->acceleration);
	//AMAX
	Evalboards.ch1.writeRegister(0, TMC5130_AMAX,  arg->acceleration);

	// Seteo desaceleracion
	//DMAX
	Evalboards.ch1.writeRegister(0, TMC5130_DMAX,  arg->acceleration);
	//D1
	Evalboards.ch1.writeRegister(0, TMC5130_D1,  arg->acceleration);

	// Seteo de registro XTARGET
	arg->displacement_z = processDipCoating.config.displacement_to_sample;
	position_target = arg->displacement_z;
	position_target = mod_coating_handlers_control_limit_(position_target);

	Evalboards.ch1.writeRegister(0, TMC5130_XTARGET,position_target  );

#if 0
	// Seteo de registro XTARGET
	arg->displacement_z = processDipCoating.config.displacement_to_sample;
	ESP_LOGI(TAG,"Desplazamiento hasta fluido en pasos:%d\r\n",arg->displacement_z);


	Evalboards.ch1.readRegister(0, TMC5130_XACTUAL, &lectura);
	Evalboards.ch1.writeRegister(0, TMC5130_XTARGET, arg->displacement_z );

	// Seteo aceleracion y velocidad
	Evalboards.ch1.writeRegister(0, TMC5130_AMAX,  arg->acceleration);
	Evalboards.ch1.writeRegister(0, TMC5130_VMAX, arg->velocity);

	// Seteo desaceleracion
	Evalboards.ch1.writeRegister(0, TMC5130_DMAX,  arg->acceleration);
#endif

	//Una vez detectado el cero realizo un movimiento fijo con 512000 pasos hacia abajo, detecto el flag que detecta XACTUAL=XTARGET
	while (processDipCoating.config.status) {

		Evalboards.ch1.readRegister(0, TMC5130_RAMPSTAT, &lectura);
		if (true == api_tmc_is_position_target(lectura))
			break;
		else if(true == api_tmc_was_vzero_detected(lectura))
			break;
		else{
		    vTaskDelay(OS_CONFIG_MOD_HANDLERS_COMMANDS_TASK_PERIOD / portTICK_PERIOD_MS);
		}
	}
	Evalboards.ch1.readRegister(0, TMC5130_XACTUAL, &lectura);

	return 0;
}


int HandlerDownLoop(processCommandArgSpin_t*	arg) {

	int32_t lectura, position_target;


	ESP_LOGI(TAG,"%s",__func__);
	ESP_LOGI(TAG,"Vel:%d",arg->velocity);
	ESP_LOGI(TAG,"Accel:%d",arg->acceleration);

	//Leo Actual
	Evalboards.ch1.readRegister(0, TMC5130_XACTUAL, &lectura);

	// velocidad
	//V1
	Evalboards.ch1.writeRegister(0, TMC5130_V1, (arg->velocity) / 2);
	//VMAX
	Evalboards.ch1.writeRegister(0, TMC5130_VMAX, arg->velocity);
	//VSTART
	Evalboards.ch1.writeRegister(0, TMC5130_VSTART, 0);
	//VSTOT
	Evalboards.ch1.writeRegister(0, TMC5130_VSTOP, 100);

	// Seteo aceleracion
	//A1
	Evalboards.ch1.writeRegister(0, TMC5130_A1,  arg->acceleration);
	//AMAX
	Evalboards.ch1.writeRegister(0, TMC5130_AMAX,  arg->acceleration);

	// Seteo desaceleracion
	//DMAX
	Evalboards.ch1.writeRegister(0, TMC5130_DMAX,  arg->acceleration);
	//D1
	Evalboards.ch1.writeRegister(0, TMC5130_D1,  arg->acceleration);

	// Seteo de registro XTARGET
	position_target = lectura + processDipCoating.config.displacement_delta_sample;
	position_target = mod_coating_handlers_control_limit_(position_target);

	Evalboards.ch1.writeRegister(0, TMC5130_XTARGET,position_target  );

#if 0
	//Leo Actual
	Evalboards.ch1.readRegister(0, TMC5130_XACTUAL, &lectura);

	// Seteo aceleracion y velocidad
	Evalboards.ch1.writeRegister(0, TMC5130_AMAX,  arg->acceleration);
	Evalboards.ch1.writeRegister(0, TMC5130_VMAX, arg->velocity);

	//desaceleracion
	Evalboards.ch1.writeRegister(0, TMC5130_DMAX,  arg->acceleration);

	// Seteo de registro XTARGET
	Evalboards.ch1.writeRegister(0, TMC5130_XTARGET, lectura + processDipCoating.config.displacement_delta_sample  );
#endif

	//Una vez detectado el cero realizo un movimiento fijo con 512000 pasos hacia abajo, detecto el flag que detecta XACTUAL=XTARGET
	while (true == processDipCoating.config.status) {

		Evalboards.ch1.readRegister(0, TMC5130_RAMPSTAT, &lectura);
		if (true == api_tmc_is_position_target(lectura))
			break;
		else if(true == api_tmc_was_vzero_detected(lectura))
			break;
		else
		  vTaskDelay(OS_CONFIG_MOD_HANDLERS_COMMANDS_TASK_PERIOD / portTICK_PERIOD_MS);
	}

	vTaskDelay(1000 / portTICK_PERIOD_MS);

	return 0;
}


int HandlerDown_without_program(processCommandArgSpin_t*	arg) {

	int32_t reg_rampstat, position_actual , position_target;
	int64_t  initial_time, final_time=0, delta_time,t_s1=0,t_s2=0;
	int32_t stage=1;
	//ESP_LOGI(TAG,"%s",__func__);


	processDipCoating.config.status=1;
	Evalboards.ch1.enableDriver(DRIVER_ENABLE);

	//Leo Actual
	Evalboards.ch1.readRegister(0, 0x21, &position_actual);

	// velocidad
	//V1
	Evalboards.ch1.writeRegister(0, TMC5130_V1, (arg->velocity) / 2);
	//VMAX
	Evalboards.ch1.writeRegister(0, TMC5130_VMAX, arg->velocity);
	//VSTART
	Evalboards.ch1.writeRegister(0, TMC5130_VSTART, 0);
	//VSTOT
	Evalboards.ch1.writeRegister(0, TMC5130_VSTOP, 100);

	// Seteo aceleracion
	//A1
	Evalboards.ch1.writeRegister(0, TMC5130_A1,  arg->acceleration);
	//AMAX
	Evalboards.ch1.writeRegister(0, TMC5130_AMAX,  arg->acceleration);

	// Seteo desaceleracion

	//DMAX
	Evalboards.ch1.writeRegister(0, TMC5130_DMAX,  arg->acceleration);
	//D1
	Evalboards.ch1.writeRegister(0, TMC5130_D1,  arg->acceleration);



	/* Seteo de registro XTARGET*/
	position_target=position_actual+arg->displacement_z;
	position_target = mod_coating_handlers_control_limit_(position_target);

	Evalboards.ch1.writeRegister(0, TMC5130_XTARGET, position_target);

#if 0 //FOR TESTING
	initial_time= board_timer_get_time_micros();
#endif



	/*Detecto el flag que detecta XACTUAL=XTARGET y apago driver*/
	while (1 == processDipCoating.config.status) {

		Evalboards.ch1.readRegister (0, TMC5130_RAMPSTAT, &reg_rampstat);

#if 0//FOR TESTING
		if (1 == stage) {
			if ((reg_rampstat & 0x00000100)) {
				t_s1 = board_timer_get_time_micros ();
				stage = 2;
			}
		}

		if (2 == stage) {
			if (!(reg_rampstat & 0x00000100)) {
				t_s2 = board_timer_get_time_micros ();
				stage = 3;
			}
		}
#endif


		/*Leo registro y comparo, velocidad zero  y  position_actual == position_target */
		if ( (true == api_tmc_is_position_target(reg_rampstat)) || (true == api_tmc_was_vzero_detected(reg_rampstat)) ){
		//if (reg_rampstat & 0x00000600){

#if 0 //FOR TESTING
			//ESP_LOGI(TAG,"Termina mov completo!, reg_rampstat:%d",reg_rampstat);
			//Evalboards.ch1.enableDriver(DRIVER_DISABLE);

			final_time = board_timer_get_time_micros();
			delta_time = final_time - initial_time;

			//ESP_LOGI(TAG," Vel:%d  Acel:%d Time Down:%d  inicial:%lld  ts1:%d  ts2: %d final:%lld ",arg->velocity, arg->acceleration,  (int32_t)delta_time, initial_time,t_s1,t_s2,final_time);
			//printf("dip_exp_2 %d,%d,%d,%d,%lld,%lld,%lld \n", 0, arg->velocity, arg->acceleration, arg->displacement_z, delta_time, t_s1 - initial_time, final_time - t_s2);
			//printf("start:%lld stop:%lld delta:%lld t_s1:%lld ts_2:%lld\r\n",initial_time, final_time, delta_time,t_s1,t_s2);

			//#dip_exp_2 id,velocidad,aceleracion,desplazamiento,delta_total,delta_acc,delta_dec \n
#endif
			break;
		}
		else {

			vTaskDelay (OS_CONFIG_MOD_HANDLERS_COMMANDS_TASK_PERIOD / portTICK_PERIOD_MS);

		}
	}

	vTaskDelay(OS_CONFIG_MOD_HANDLERS_COMMANDS_TASK_PERIOD_LONG / portTICK_PERIOD_MS);
	processDipCoating.config.status=0;
	Evalboards.ch1.enableDriver(DRIVER_DISABLE);

#if 0 //FOR TESTING
//	Evalboards.ch1.readRegister(0, 0x21, &position_actual);
//	Evalboards.ch1.readRegister(0, 0x2D, &position_target);
#endif

	return 0;

}

int HandlerWait(processCommandArgSpin_t*	arg) {

	ESP_LOGI(TAG,"%s",__func__);
	Evalboards.ch1.enableDriver(DRIVER_DISABLE);
	vTaskDelay((processDipCoating.command[1].argument.wait.time * 1000) / portTICK_PERIOD_MS);
	//vTaskDelay(1000 / portTICK_PERIOD_MS);
	Evalboards.ch1.enableDriver(DRIVER_ENABLE);
	return 0;
}

int HandlerWaitDown(processCommandArgSpin_t*	arg) {

	ESP_LOGI(TAG,"%s",__func__);
	Evalboards.ch1.enableDriver(DRIVER_DISABLE);
	vTaskDelay((processDipCoating.command[3].argument.wait.time * 1000) / portTICK_PERIOD_MS);
	Evalboards.ch1.enableDriver(DRIVER_ENABLE);
	return 0;
}


int HandlerWaitUp(processCommandArgSpin_t*	arg) {

	ESP_LOGI(TAG,"%s",__func__);
	Evalboards.ch1.enableDriver(DRIVER_DISABLE);
	vTaskDelay((processDipCoating.command[5].argument.wait.time * 1000) / portTICK_PERIOD_MS);
	Evalboards.ch1.enableDriver(DRIVER_ENABLE);
	return 0;
}


int HandlerRun() {

	ESP_LOGI(TAG,"%s",__func__);
	Evalboards.ch1.enableDriver(DRIVER_ENABLE);
	processDipCoating.run_protocol=true;
	return 0;
}


int HandlerStop(processCommandArgSpin_t*	arg) {

#if 0
	ESP_LOGI(TAG,"%s",__func__);
	ESP_LOGI(TAG,"Vel:%d",arg->velocity);
	ESP_LOGI(TAG,"Accel:%d",arg->acceleration);


	// Seteo aceleracion y velocidad en cero
	Evalboards.ch1.writeRegister(0, 0x26,  arg->acceleration);
	Evalboards.ch1.writeRegister(0, 0x27, arg->velocity);
	Evalboards.ch1.enableDriver(DRIVER_DISABLE);

	return 0;
#endif

	int32_t lectura, lecturaBanderas;

	Evalboards.ch1.enableDriver(DRIVER_DISABLE);

	Evalboards.ch1.writeRegister(0, TMC5130_AMAX,  arg->acceleration);
	Evalboards.ch1.writeRegister(0, TMC5130_VMAX, arg->velocity);


	processDipCoating.run_protocol= false;
	processDipCoating.config.status = 0;

	Evalboards.ch1.readRegister(0, TMC5130_XACTUAL, &lectura);

	Evalboards.ch1.writeRegister(0, TMC5130_XACTUAL, lectura);
	Evalboards.ch1.writeRegister(0, TMC5130_XTARGET, lectura);

//TODO 	ver este tema de banderas
	Evalboards.ch1.readRegister(0, TMC5130_RAMPSTAT, &lecturaBanderas);

//	Evalboards.ch1.writeRegister(0, 0x21, lectura);
//	Evalboards.ch1.writeRegister(0, 0x2D, lectura);
//	Evalboards.ch1.readRegister(0, 0x35, &lecturaBanderas);

return 0;

}


int HandlerFinish(processCommandArgSpin_t*	arg) {

	ESP_LOGI(TAG,"%s",__func__);
	Evalboards.ch1.enableDriver(DRIVER_DISABLE);

	return 0;
}

int HandlerREADDATA() {

	int32_t lectura, i;
	/*Lectura de datos configurados para el funcionamiento de la maquina*/

	for (i=0; i<8 ;i++){
		ESP_LOGI(TAG,"%d %d %d" ,processDipCoating.command[i].commandnumber,processDipCoating.command[i].argument.spin.velocity ,processDipCoating.command[i].argument.spin.acceleration);
	}
	ESP_LOGI(TAG,"Depth Sample (steps):%d",processDipCoating.config.displacement_delta_sample);
	ESP_LOGI(TAG,"Displacement to Sample (steps):%d",processDipCoating.config.displacement_to_sample);
	ESP_LOGI(TAG,"STATUS:%d",processDipCoating.config.status);

	//Leo Actual

	Evalboards.ch1.readRegister(0, TMC5130_XACTUAL, &lectura);
	ESP_LOGI(TAG,"Posicion ACTUAL TARGET:%d", lectura);

	Evalboards.ch1.readRegister(0, TMC5130_XTARGET, &lectura);
	ESP_LOGI(TAG,"Posicion  XTARGET:%d", lectura);


	return 0;
}


int HandlerENA_DRIVER() {

	ESP_LOGI(TAG,"%s",__func__);
	Evalboards.ch1.enableDriver(DRIVER_ENABLE);

	return 0;
}

int HandlerDIS_DRIVER() {

	ESP_LOGI(TAG,"%s",__func__);
	Evalboards.ch1.enableDriver(DRIVER_DISABLE);

	return 0;
}



int HandlerCERO_SAMPLE() {

	int32_t lectura;

	Evalboards.ch1.readRegister(0, TMC5130_XACTUAL, &lectura);     //
	ESP_LOGI(TAG,"Cero_Sample(mm):%d", lectura / MOD_COATING_HANDLERS_STEPS_PER_MILLIMETER  );    //On INS machine 12737   --> 1 mm

	processDipCoating.config.displacement_to_sample=lectura;

	return 0;
}


int HandlerDELTADIP() {

	ESP_LOGI(TAG,"%s",__func__);
	return 0;
}


/* La función mod_coating_handlers_control_limit_ controla los limites mecanicos de la maquina, superior e inferior, el carro no podra
 * desplazarce fuera de estos limites.
*/

int32 mod_coating_handlers_control_limit_(int32 value_limit){
	//ESP_LOGI(TAG,"%s",__func__);

	//Evalboards.ch1.readRegister(0, 0x21, &lectura);
	//ESP_LOGI(TAG,"Posicion ACTUAL :%d", lectura);
	//Evalboards.ch1.readRegister(0, 0x2D, &lecturaXtarget);
	//ESP_LOGI(TAG,"Posicion XTARGET :%d", lecturaXtarget);

		if (value_limit  <=  MOD_COATING_HANDLERS_UPPER_LIMIT  ){    //   Decimal:63686

				//Evalboards.ch1.enableDriver(DRIVER_DISABLE);
				//processDipCoating.config.cero_machine=1;

				return MOD_COATING_HANDLERS_UPPER_LIMIT;
			}
		else if ( value_limit >= processDipCoating.config.new_botton_limit ){		//	Decimal:4075910

				//Evalboards.ch1.enableDriver(DRIVER_DISABLE);
				//processDipCoating.config.cero_machine=1;
				return processDipCoating.config.new_botton_limit;
			}
		else return value_limit;
}


int HandlerRESET() {

	ESP_LOGI(TAG, "Restart....");
	board_reset_restart();
	return 0;
}




void mod_coating_handlers_init(){}
void mod_coating_handlers_deinit(){}
void mod_coating_handlers_loop(){}

/********************** end of file ******************************************/

/** @}Final Doxygen */
