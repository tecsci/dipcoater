/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		app_coating.c
* @date		24 jun. 2021
* @author	
*		-Martin Abel Gambarotta   <magambarotta@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "app_coating.h"
#include "mod_coating_process.h"
#include "api.h"

#include "esp_log.h"
#include "os_config.h"

#include "TMCMotionController.h"
#include "TMC_Board.h"
#include "TMC5130.h"

#include "TMCL.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/
static const char *TAG = "app_coating";
/********************** internal data definition *****************************/

/********************** external data definition *****************************/

#if 1
//xQueue  Complete  Program
QueueHandle_t xQueueCompleteProgram;

//xQueue  Individual Command
QueueHandle_t xQueueIndividualCommand;

//xQueue  Individual Command
QueueHandle_t xQueueIndividualCommand_Stop;

#endif

extern void TMC5130_init(void);

bool app_coating_get_status_machine(void){
	return processDipCoating.run_protocol;
}

bool app_coating_get_status_state_machine(void){
	return processDipCoating.config.status;
}


bool app_coating_get_program_name(char* p_name){
	strcpy(p_name,processDipCoating.last_program_name);
	return true;
}


int64_t app_coating_get_last_start_time_program(void){
	return processDipCoating.ms_last_start_time;
}


/********************** internal functions definition ************************/
static void app_coating_config_stepper_motor_ (void);

/********************** external functions definition ************************/
void app_coating_init()
{
	ESP_LOGI(TAG,"%s",__func__);

	xQueueCompleteProgram= xQueueCreate( 1, sizeof( api_program_manager_program_t) );

  /* Creo Cola de mensajes para los comandos individuales recibidos por pantalla
   * los comandos pueden llegar por el puerto serie o por puerto serial desde la pantalla
   */
  xQueueIndividualCommand= xQueueCreate( 1, sizeof( processCommand_t) );

  xQueueIndividualCommand_Stop= xQueueCreate( 1, sizeof( processCommand_t) );




	xTaskCreate(app_coating_loop, "coating task" 	// A name just for humans
			, OS_CONFIG_APP_COATING_TASK_SIZE 			// This stack size can be checked & adjusted by reading the Stack Highwater
			, NULL, OS_CONFIG_APP_COATING_TASK_PRIORITY 		// Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
			, NULL);

}

void app_coating_deinit(){}
void app_coating_loop () {
	/*Inicializacion  Dip Coater struct Config*/
	mod_coating_process_init ();

	/*Inicializacion de funciones de la API Trinamic*/
	tmcmotioncontroller_init ();
	TMC5130_init ();

	/*Inicializacion  Dip Coater Hardware Config*/
	app_coating_config_stepper_motor_ ();


	//vTaskDelay (100 / portTICK_PERIOD_MS);

	for (;;) {
		//ESP_LOGI(TAG,"%s",__func__);

		/*Check entry commads*/
		mod_coating_process_loop();
		vTaskDelay (OS_CONFIG_APP_COATING_TASK_PERIOD / portTICK_PERIOD_MS);

	}
}


static void app_coating_config_stepper_motor_ (void)
{

	Evalboards.ch1.enableDriver (DRIVER_USE_GLOBAL_ENABLE);

	Evalboards.ch1.writeRegister(0,0x00, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 0 = 0x00(GCONF)
	Evalboards.ch1.writeRegister(0,0x03, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 1 = 0x03(SLAVECONF)
	Evalboards.ch1.writeRegister(0,0x05, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 2 = 0x05(X_COMPARE)
	Evalboards.ch1.writeRegister(0,0x10, 	0x00070A01); 		// writing value 0x00070A00 = 461312 = 0.0 to address 3 = 0x10(IHOLD_IRUN)
	Evalboards.ch1.writeRegister(0,0x11, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 4 = 0x11(TPOWERDOWN)
	Evalboards.ch1.writeRegister(0,0x13, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 5 = 0x13(TPWMTHRS)
	Evalboards.ch1.writeRegister(0,0x14, 	0x00000D1B); 		// writing value 0x00000D1B = 3355 = 0.0 to address 6 = 0x14(TCOOLTHRS)
	Evalboards.ch1.writeRegister(0,0x15, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 7 = 0x15(THIGH)
	Evalboards.ch1.writeRegister(0,0x20, 	0x00000000); 		// writing value 0x00000001 = 1 = 0.0 to address 8 = 0x20(RAMPMODE)
	Evalboards.ch1.writeRegister(0,0x21, 	0x00000000); 		// writing value 0xFFFA6B8C = 0 = 0.0 to address 9 = 0x21(XACTUAL)
	Evalboards.ch1.writeRegister(0,0x23, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 10 = 0x23(VSTART)
	Evalboards.ch1.writeRegister(0,0x24, 	0x000003E8); 		// writing value 0x000003E8 = 1000 = 0.0 to address 11 = 0x24(A1)
	Evalboards.ch1.writeRegister(0,0x25, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 12 = 0x25(V1)
	Evalboards.ch1.writeRegister(0,0x26, 	0x000003E8); 		// writing value 0x000003E8 = 1000 = 0.0 to address 13 = 0x26(AMAX)
	Evalboards.ch1.writeRegister(0,0x27, 	0x0000C350); 		// writing value 0x00000000 = 0 = 0.0 to address 14 = 0x27(VMAX)
	Evalboards.ch1.writeRegister(0,0x28, 	0x000002BC); 		// writing value 0x000002BC = 700 = 0.0 to address 15 = 0x28(DMAX)
	Evalboards.ch1.writeRegister(0,0x2A, 	0x00000578); 		// writing value 0x00000578 = 1400 = 0.0 to address 16 = 0x2A(D1)
	Evalboards.ch1.writeRegister(0,0x2B, 	0x0000000A); 		// writing value 0x0000000A = 10 = 0.0 to address 17 = 0x2B(VSTOP)
	Evalboards.ch1.writeRegister(0,0x2C, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 18 = 0x2C(TZEROWAIT)
	Evalboards.ch1.writeRegister(0,0x2D, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 19 = 0x2D(XTARGET)
	Evalboards.ch1.writeRegister(0,0x33, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 20 = 0x33(VDCMIN)
	Evalboards.ch1.writeRegister(0,0x34, 	0x00000400); 		// writing value 0x00000400 = 1024 = 0.0 to address 21 = 0x34(SW_MODE)
	Evalboards.ch1.writeRegister(0,0x38, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 22 = 0x38(ENCMODE)
	Evalboards.ch1.writeRegister(0,0x39, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 23 = 0x39(X_ENC)
	Evalboards.ch1.writeRegister(0,0x3A, 	0x00010000); 		// writing value 0x00010000 = 65536 = 0.0 to address 24 = 0x3A(ENC_CONST)
	Evalboards.ch1.writeRegister(0,0x60, 	0xAAAAB554); 		// writing value 0xAAAAB554 = 0 = 0.0 to address 25 = 0x60(MSLUT[0])
	Evalboards.ch1.writeRegister(0,0x61, 	0x4A9554AA); 		// writing value 0x4A9554AA = 1251300522 = 0.0 to address 26 = 0x61(MSLUT[1])
	Evalboards.ch1.writeRegister(0,0x62, 	0x24492929); 		// writing value 0x24492929 = 608774441 = 0.0 to address 27 = 0x62(MSLUT[2])
	Evalboards.ch1.writeRegister(0,0x63, 	0x10104222); 		// writing value 0x10104222 = 269500962 = 0.0 to address 28 = 0x63(MSLUT[3])
	Evalboards.ch1.writeRegister(0,0x64, 	0xFBFFFFFF); 		// writing value 0xFBFFFFFF = 0 = 0.0 to address 29 = 0x64(MSLUT[4])
	Evalboards.ch1.writeRegister(0,0x65, 	0xB5BB777D); 		// writing value 0xB5BB777D = 0 = 0.0 to address 30 = 0x65(MSLUT[5])
	Evalboards.ch1.writeRegister(0,0x66, 	0x49295556); 		// writing value 0x49295556 = 1227445590 = 0.0 to address 31 = 0x66(MSLUT[6])
	Evalboards.ch1.writeRegister(0,0x67, 	0x00404222); 		// writing value 0x00404222 = 4211234 = 0.0 to address 32 = 0x67(MSLUT[7])
	Evalboards.ch1.writeRegister(0,0x68, 	0xFFFF8056); 		// writing value 0xFFFF8056 = 0 = 0.0 to address 33 = 0x68(MSLUTSEL)
	Evalboards.ch1.writeRegister(0,0x69, 	0x00F70000); 		// writing value 0x00F70000 = 16187392 = 0.0 to address 34 = 0x69(MSLUTSTART)
	Evalboards.ch1.writeRegister(0,0x6C, 	0x000101D5); 		// writing value 0x000101D5 = 66005 = 0.0 to address 35 = 0x6C(CHOPCONF)

	//Sensitivity StallGuard
	//si achico el valor, la deteccion es mas sensible, es decir corta por menos exceso de corriente
	//Evalboards.ch1.writeRegister(0,0x6D, 	0x01030000); 		// writing value 0x01030000 = 16973824 = 0.0 to address 36 = 0x6D(COOLCONF)
	//Evalboards.ch1.writeRegister(0,0x6D, 	0x01020000); 		// writing value 0x01030000 = 16973824 = 0.0 to address 36 = 0x6D(COOLCONF)
	//Evalboards.ch1.writeRegister(0,0x6D, 	0x01040000); 		// writing value 0x01030000 = 16973824 = 0.0 to address 36 = 0x6D(COOLCONF)

	////Version CNEA Final

	//Evalboards.ch1.writeRegister(0,0x6D, 	0x0081B320); 		// writing value 0x01030000 = 8.500.000 = 0.0 to address 36 = 0x6D(COOLCONF)  SALTA
	Evalboards.ch1.writeRegister(0,0x6D, 	0x00827670); 		// writing value 0x01030000 = 8.550.000 = 0.0 to address 36 = 0x6D(COOLCONF)  SE TRABA UN POCO Y BAJA
	//Evalboards.ch1.writeRegister(0,0x6D, 	0x008339C0); 		// writing value 0x01030000 = 8.600.000 = 0.0 to address 36 = 0x6D(COOLCONF)  SE TRABA UN POCO Y BAJA
	//Evalboards.ch1.writeRegister(0,0x6D, 	0x008583B0); 		// writing value 0x01030000 = 8.750.000 = 0.0 to address 36 = 0x6D(COOLCONF)  NO SALTA
	//Evalboards.ch1.writeRegister(0,0x6D, 	0x00864700); 		// writing value 0x01030000 = 8.800.000 = 0.0 to address 36 = 0x6D(COOLCONF)  NO SALTA
	//Evalboards.ch1.writeRegister(0,0x6D, 	0x00895440); 		// writing value 0x01030000 = 9.000.000 = 0.0 to address 36 = 0x6D(COOLCONF)  NO SALTA


	//Evalboards.ch1.writeRegister(0,0x6D, 	0x008DE820); 		// writing value 0x01030000 = 9.350.000 = 0.0 to address 36 = 0x6D(COOLCONF)
	//Evalboards.ch1.writeRegister(0,0x6D, 	0x0040C000); 		// writing value 0x01030000 = 4.243.456 = 0.0 to address 36 = 0x6D(COOLCONF)

	//Evalboards.ch1.writeRegister(0,0x6D, 	0x00000000); 		// writing value 0x01030000 = 16973824 = 0.0 to address 36 = 0x6D(COOLCONF)   //Detecta cuando esta abajo

#if 0
	Evalboards.ch1.writeRegister(0,TMC5130_COOLCONF, 	0xB << 10);
#endif
	//Evalboards.ch1.writeRegister(0,TMC5130_COOLCONF, 	0x00005800);   //22528
	//Evalboards.ch1.writeRegister(0,TMC5130_COOLCONF, 	0x00818000);   //8486912
	//Evalboards.ch1.writeRegister(0,TMC5130_COOLCONF, 	0x01030000);   //16973824

	//Version FAN Final
	//Evalboards.ch1.writeRegister(0,TMC5130_COOLCONF, 	0x050F0000);   //84869120

	Evalboards.ch1.writeRegister(0,0x6E, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 37 = 0x6E(DCCTRL)
	Evalboards.ch1.writeRegister(0,0x70, 	0x000504C8); 		// writing value 0x000504C8 = 328904 = 0.0 to address 38 = 0x70(PWMCONF)
	Evalboards.ch1.writeRegister(0,0x72, 	0x00000000); 		// writing value 0x00000000 = 0 = 0.0 to address 39 = 0x72(ENCM_CTRL)

	Evalboards.ch1.enableDriver (DRIVER_DISABLE);

}

/********************** end of file ******************************************/

/** @}Final Doxygen */
