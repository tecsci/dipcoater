/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		mod_coating_handlers.h
* @date		20 jul. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef COMPONENTS_APP_COATING_INCLUDE_MOD_COATING_HANDLERS_H_
#define COMPONENTS_APP_COATING_INCLUDE_MOD_COATING_HANDLERS_H_
/********************** inclusions *******************************************/
#include "app_coating.h"
#include "mod_coating_process.h"

/********************** macros ***********************************************/

#define MOD_COATING_HANDLERS_UPPER_LIMIT						MACHINE_CONTROL_MECANICAL_UPPER_LIMIT
#define MOD_COATING_HANDLERS_LOWER_LIMIT 						MACHINE_CONTROL_MECANICAL_LOWER_LIMIT

#define MOD_COATING_HANDLERS_STEPS_PER_MILLIMETER		MACHINE_STEPS_PER_MILLIMETER

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/
int HandlerCeroMachine(processCommandArgSpin_t	*arg);
int HandlerSpin(processCommandArgSpin_t		*arg);

//int HandlerUp(processCommandArgSpin_t 	*arg);
int HandlerUpLoop(processCommandArgSpin_t 	*arg);
int HandlerUp_without_program(processCommandArgSpin_t*	arg);

int HandlerDownUntil(processCommandArgSpin_t *arg);
int HandlerDownLoop(processCommandArgSpin_t *arg);
int HandlerDown_without_program(processCommandArgSpin_t*	arg);



int HandlerWait(processCommandArgSpin_t *arg);
int HandlerWaitDown(processCommandArgSpin_t *arg);
int HandlerWaitUp(processCommandArgSpin_t *arg);
int HandlerStop(processCommandArgSpin_t *arg);
int HandlerFinish(processCommandArgSpin_t *arg);
int HandlerRun();

int HandlerENA_DRIVER(void);
int HandlerDIS_DRIVER(void);
int HandlerCERO_SAMPLE(void);
int HandlerDELTADIP(void);

int HandlerRESET(void);

//Handler para realizar TESTS
int HandlerREADDATA(void);

void mod_coating_handlers_control_change_bottom_limit(float value);
float  mod_coating_handlers_control_get_bottom_limit(void);

void mod_coating_handlers_init(void);
void mod_coating_handlers_deinit(void);
void mod_coating_handlers_loop(void);

#endif /* COMPONENTS_APP_COATING_INCLUDE_MOD_COATING_HANDLERS_H_ */
/** @}Final Doxygen */

