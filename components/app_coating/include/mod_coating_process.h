/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		mod_coating_process.h
* @date		20 jul. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef COMPONENTS_APP_COATING_INCLUDE_MOD_COATING_PROCESS_H_
#define COMPONENTS_APP_COATING_INCLUDE_MOD_COATING_PROCESS_H_
/********************** inclusions *******************************************/
#include "stdint.h"
/********************** macros ***********************************************/

/*  Estructura del Proceso + Comando
 *
 *
 *
 *
 *						|Arreglo de comandos del tipo  processCommand_t |
 *						|												|
 *						|-----------------------------------------------|
 *						|												|
 *						|Estructura de configuracion  processConfig_t	|
 * Estructura process_t	|												|
 *						|-----------------------------------------------|
 *						|												|
 *						|Estructura de estado  processState_t			|
 *						|												|
 *						|-----------------------------------------------|
 *
 *
 *
 */

/********************** typedef **********************************************/

#if 0

typedef struct {
	float values[4];
} processCommandArg4Float_t;



typedef struct {
	uint32_t values[4];
} processCommandArg4Int32_t;

#endif

typedef struct {
	int val;
} processCommandArgUint8_t;

typedef struct {
	uint32_t velocity;
	uint32_t acceleration;
	uint32_t displacement_z;

} processCommandArgSpin_t;

typedef struct {
	uint32_t time;
} processCommandArgWait_t;

typedef enum {
	PROCESS_COMMAND_CERO_MACHINE,	/*0*/
	PROCESS_COMMAND_SPIN,					/*1*/
	PROCESS_COMMAND_UP,						/*2*/
	PROCESS_COMMAND_DOWN,					/*3*/
	PROCESS_COMMAND_WAIT,					/*4*/
	PROCESS_COMMAND_STOP,					/*5*/
	PROCESS_COMMAND_START,				/*6*/
	PROCESS_COMMAND_FINISH,				/*7*/
	PROCESS_COMMAND_LOOP,					/*8*/
	PROCESS_COMMAND__EMPTY,				/*9*/
	PROCESS_COMMAND_RUN,
	PROCESS_COMMAND_UPFAST,
	PROCESS_COMMAND_UPSLOW,
	PROCESS_COMMAND_DOWNFAST,
	PROCESS_COMMAND_DOWNSLOW,
	PROCESS_COMMAND_READDATA,
	PROCESS_COMMAND_ENA_DRIVER,
	PROCESS_COMMAND_DIS_DRIVER,
	PROCESS_COMMAND_CERO_SAMPLE,
	PROCESS_COMMAND_DELTADIP,
	PROCESS_COMMAND_RESET,
	PROCESS_COMMAND_N  						/*21*/

} proccesCommandNumber_t;


typedef int (*processCommandHandler_t)(processCommandArgSpin_t* arg);


typedef union {
	processCommandArgSpin_t spin;
	processCommandArgWait_t wait;
//	processCommandArg4Float_t floats;
	processCommandArgUint8_t value;
} processCommandArgument_t;

typedef struct {
	proccesCommandNumber_t commandnumber;
	processCommandHandler_t fpcommandhandler;

	union {
		processCommandArgSpin_t spin;
		processCommandArgWait_t wait;
//		processCommandArg4Float_t floats;
		processCommandArgUint8_t value;
	} argument;

} processCommand_t;

typedef struct {

	/*Configuracion ->  status  -> 1  cuando se esta ejecutando un programa o se corre un comando individual   -> 0 cuando no se esta ejecutando ningun programa        desplazamiento en eje Y segun pasos del motor   */
	uint8_t	status;

	/**/
	bool cero_machine;

	uint32_t displacement_delta_sample;    	/*Depende del tamaño de la muestra*/
	uint32_t displacement_to_sample;    	/*Depende del tamaño del recipiente*/

	int32_t  new_botton_limit;


} processConfig_t;

typedef struct {

	uint8_t commandIndex;
	uint8_t commandActualIndex;
	uint8_t flags;

} processState_t;

typedef struct {
	processConfig_t 		config; 			/*Parametros estructurales de la maquina   -  Limites*/
	processCommand_t*		command;
	uint8_t							commandlen;
	processState_t 			state; 				/*Exteriorizar el estado actual (ejecutando un comando/programa o sin trabajar)*/


	int64_t							ms_last_start_time;
	bool								run_protocol;
	char								last_program_name[30];

} process_t;

/********************** external data declaration ****************************/


extern process_t processDipCoating;
extern processCommand_t cmdProcessCustom[];

/********************** external functions declaration ***********************/


void mod_coating_process_run(process_t *process);
void mod_coating_process_cero_machine();
void mod_coating_process_command_stop();


void mod_coating_process_init(void);
void mod_coating_process_deinit(void);
void mod_coating_process_loop(void);

#endif /* COMPONENTS_APP_COATING_INCLUDE_MOD_COATING_PROCESS_H_ */
/** @}Final Doxygen */

