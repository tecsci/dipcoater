/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		mod_coating_emergency.h
* @date		20 jul. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef COMPONENTS_APP_COATING_INCLUDE_MOD_COATING_EMERGENCY_H_
#define COMPONENTS_APP_COATING_INCLUDE_MOD_COATING_EMERGENCY_H_
/********************** inclusions *******************************************/
#include "stdint.h"
/********************** macros ***********************************************/
/********************** typedef **********************************************/
/********************** external data declaration ****************************/
/********************** external functions declaration ***********************/

void mod_coating_emergency_init(void);
void mod_coating_emergency_deinit(void);
void mod_coating_emergency_loop(void);

#endif /* COMPONENTS_APP_COATING_INCLUDE_MOD_COATING_PROCESS_H_ */
/** @}Final Doxygen */

