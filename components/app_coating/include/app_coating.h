/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		app_coating.h
* @date		24 jun. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef COMPONENTS_APP_INCLUDE_APP_COATING_H_
#define COMPONENTS_APP_INCLUDE_APP_COATING_H_
/********************** inclusions *******************************************/

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

//xQueue witch complete protocol program
extern QueueHandle_t xQueueCompleteProgram;

//xQueue witch individual command
extern QueueHandle_t xQueueIndividualCommand;

//xQueue witch individual command
extern QueueHandle_t xQueueIndividualCommand_Stop;



/********************** external functions declaration ***********************/

bool app_coating_get_status_machine(void);
bool app_coating_get_status_state_machine(void);

bool app_coating_get_program_name(char* p_name);
int64_t app_coating_get_last_start_time_program(void);
uint32_t app_coating_get_program_loops(void);

void app_coating_init();
void app_coating_deinit();
void app_coating_loop();

#endif /* COMPONENTS_APP_INCLUDE_APP_COATING_H_ */
/** @}Final Doxygen */

