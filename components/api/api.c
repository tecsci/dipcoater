/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		api.c
* @date		15 jun. 2021
* @author	
*		-Martin Abel Gambarotta   <magambarotta@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "api.h"

#include <stdio.h>

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/
void api_init (void)
{
	api_gpio_init();
	api_serial_init();
	api_spi_init();
	api_tmc_init();
	api_program_manager_init();
	api_stone_hmi_init();
	api_bosch_bme280_init();
}

void api_deinit (void)
{
	api_bosch_bme280_deinit();
	api_stone_hmi_deinit();
	api_program_manager_deinit();
	api_tmc_deinit();
	api_gpio_deinit();
	api_serial_deinit();
	api_spi_deinit();
}
void api_loop (void)
{
  api_gpio_loop();
  api_serial_loop();
  api_spi_loop();
  api_tmc_loop();
  api_program_manager_loop();
  api_stone_hmi_loop();
  api_bosch_bme280_loop();
}
/********************** end of file ******************************************/

/** @}Final Doxygen */
