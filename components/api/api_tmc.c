/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* Copyright (C) <2021>  <Martin Abel Gambarotta   magambarotta@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		api_tmc.c
* @date		14 jun. 2021
* @author	
*		-Martin Abel Gambarotta   magambarotta@gmail.com
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include <stdio.h>
#include <stdlib.h>

#include "api_tmc.h"
#include "api_gpio.h"
#include "api_spi.h"

#include "hardware.h"
#include "machine.h"
#include "esp_log.h"

#include "TMC_Board.h"
#include "TMC5130_Register.h"


/********************** macros and definitions *******************************/
#define API_TMC_DRIVER_ENABLE_PORT HARDWARE_ESP32_TMC_DRIVER_ENABLE

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/
static const char*TAG ="api_tmc";
/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/
float api_tmc_get_actual_position(void)
{
	int rx_register;
	Evalboards.ch1.readRegister (0, TMC5130_XACTUAL, &rx_register);
	return ((float)rx_register/ (float)MACHINE_STEPS_PER_MILLIMETER);
}

void api_tmc_driver_enable(bool state)
{
  if (true == state)
    {
      api_gpio_write(API_TMC_DRIVER_ENABLE_PORT,true);
    }
  else api_gpio_write(API_TMC_DRIVER_ENABLE_PORT,false);
}

void api_tmc_write_read(uint8_t *tx, uint8_t *rx, size_t length)
{
  api_spi_write_read(API_SPI_2,tx,rx,length);
}

bool api_tmc_was_stallguard_detected(int32_t readed_rampstat_reg){
	if (readed_rampstat_reg & 0x0000040) {
		return true;
	}

	return false;
}
bool api_tmc_was_vzero_detected(int32_t readed_rampstat_reg){
	if (readed_rampstat_reg & 0x0000400) {
		return true;
	}

	return false;
}
bool api_tmc_was_position_reached(int32_t readed_rampstat_reg){
	if (readed_rampstat_reg & 0x0000080) {
		return true;
	}

	return false;
}

bool api_tmc_is_position_target(int32_t readed_rampstat_reg){
	if (readed_rampstat_reg & 0x0000200) {
		return true;
	}

	return false;
}

bool api_tmc_was_velocity_reached(int32_t readed_rampstat_reg){
	if (readed_rampstat_reg & 0x0000100) {
		return true;
	}

	return false;
}


void api_tmc_init (void){}
void api_tmc_deinit (void){}
void api_tmc_loop (void){}

/********************** end of file ******************************************/

/** @}Final Doxygen */
