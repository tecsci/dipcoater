/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		xsx.c
* @date		3 feb. 2022
* @author
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v2.0.0
*
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/

#include "api_program_manager.h"
#include "board_spiffs.h"
#include "rsc_json_lib.h"
#include "esp_log.h"


/********************** macros and definitions *******************************/
#define API_PROGRAM_MAX_INT_TO_STRING 	(8)
#define API_PROGRAM_MAX_PROGRAM_INDEX   (50)
#define API_PROGRAM_DATA_BUFF_SZ 				(512)
#define API_PROGRAM_FILENAME_BUFF_SZ 		(50)
#define API_PROGRAM_FILENAME_BASE 	    "/spiffs/program_"
/********************** internal data declaration ****************************/

static struct{

	char aux_data_buff[API_PROGRAM_DATA_BUFF_SZ];
	char aux_filename_buff[API_PROGRAM_FILENAME_BUFF_SZ];

}self_;


static const char *TAG = "api_program_manager";

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

bool user_serialize_func_(api_program_manager_program_t * program, char * buffer, uint32_t buffer_size){
	ESP_LOGI(TAG,"%s",__func__);

			rsc_json_lib_t *new_json;
			rsc_json_lib_create(&new_json);
			char aux_itoa[API_PROGRAM_MAX_INT_TO_STRING];


			if (false == rsc_json_lib_add_data(new_json, "name" , program->name)){
				ESP_LOGE(TAG, "error adding");
				rsc_json_lib_delete(new_json);
				return false;
			}

			//add field

			itoa(program->velocity_down,aux_itoa,10);
			if (false == rsc_json_lib_add_data(new_json, "velocity_down" , aux_itoa)){
				ESP_LOGE(TAG, "error adding velocity_down");
				rsc_json_lib_delete(new_json);
				return false;
			}

			itoa(program->acceleration_down,aux_itoa,10);
			if (false == rsc_json_lib_add_data(new_json, "acceleration_down" , aux_itoa)){
				ESP_LOGE(TAG, "error adding acceleration_down");
				rsc_json_lib_delete(new_json);
				return false;
			}


			itoa(program->wait_down,aux_itoa,10);
			if (false == rsc_json_lib_add_data(new_json, "wait_down" , aux_itoa)){
				ESP_LOGE(TAG, "error adding wait_down");
				rsc_json_lib_delete(new_json);
				return false;
			}

			itoa(program->velocity_up,aux_itoa,10);
			if (false == rsc_json_lib_add_data(new_json, "velocity_up" , aux_itoa)){
				ESP_LOGE(TAG, "error adding velocity_up");
				rsc_json_lib_delete(new_json);
				return false;
			}


			itoa(program->acceleration_up,aux_itoa,10);
			if (false == rsc_json_lib_add_data(new_json, "acceleration_up" , aux_itoa)){
				ESP_LOGE(TAG, "error adding acceleration_up");
				rsc_json_lib_delete(new_json);
				return false;
			}


			itoa(program->wait_up,aux_itoa,10);
			if (false == rsc_json_lib_add_data(new_json, "wait_up" , aux_itoa)){
				ESP_LOGE(TAG, "error adding wait_up");
				rsc_json_lib_delete(new_json);
				return false;
			}



			itoa(program->depth_sample,aux_itoa,10);
			if (false == rsc_json_lib_add_data(new_json, "depth_sample" , aux_itoa)){
				ESP_LOGE(TAG, "error adding depth_sample");
				rsc_json_lib_delete(new_json);
				return false;
			}

			itoa(program->displacement_to_sample,aux_itoa,10);
			if (false == rsc_json_lib_add_data(new_json, "displacement_to_sample" , aux_itoa)){
				ESP_LOGE(TAG, "error adding displacement_to_sample");
				rsc_json_lib_delete(new_json);
				return false;
			}

			itoa(program->loops,aux_itoa,10);
			if (false == rsc_json_lib_add_data(new_json, "loops" , aux_itoa)){
				ESP_LOGE(TAG, "error adding loops");
				rsc_json_lib_delete(new_json);
				return false;
			}

			//serializar
			if( false == rsc_json_lib_serialize(new_json, buffer, buffer_size)){
				ESP_LOGE(TAG, "error serializing");
				rsc_json_lib_delete(new_json);
				return false;
			}

			rsc_json_lib_delete(new_json);
			return true;
}

bool user_deserialize_func_(api_program_manager_program_t * program, char * buffer, uint32_t buffer_size){
	ESP_LOGI(TAG,"%s",__func__);
	rsc_json_lib_t *json_parseado;
	rsc_json_lib_deserialize(&json_parseado, buffer, buffer_size);
	char aux_atoi[API_PROGRAM_MAX_INT_TO_STRING];


	if(false == rsc_json_lib_get_item_data_by_name(json_parseado, "name" , program->name, API_PROGRAM_NAME_SIZE)){
		ESP_LOGI(TAG,"name:%s",program->name);
		rsc_json_lib_delete(json_parseado);
		return false;
	}



	if(false == rsc_json_lib_get_item_data_by_name(json_parseado,  "velocity_down", aux_atoi, API_PROGRAM_MAX_INT_TO_STRING)){
		ESP_LOGI(TAG,"%s",aux_atoi);
		rsc_json_lib_delete(json_parseado);
		return false;
	}
	program->velocity_down =(uint32_t)atoi(aux_atoi);



	if(false == rsc_json_lib_get_item_data_by_name(json_parseado,  "acceleration_down", aux_atoi, API_PROGRAM_MAX_INT_TO_STRING)){
		rsc_json_lib_delete(json_parseado);
		return false;
	}
	program->acceleration_down =(uint32_t)atoi(aux_atoi);

	if(false == rsc_json_lib_get_item_data_by_name(json_parseado,  "wait_down", aux_atoi, API_PROGRAM_MAX_INT_TO_STRING)){
		rsc_json_lib_delete(json_parseado);
		return false;
	}
	program->wait_down =(uint32_t)atoi(aux_atoi);

	if(false == rsc_json_lib_get_item_data_by_name(json_parseado,  "velocity_up", aux_atoi, API_PROGRAM_MAX_INT_TO_STRING)){
		rsc_json_lib_delete(json_parseado);
		return false;
	}
	program->velocity_up =(uint32_t)atoi(aux_atoi);

	if(false == rsc_json_lib_get_item_data_by_name(json_parseado,  "acceleration_up", aux_atoi, API_PROGRAM_MAX_INT_TO_STRING)){
		rsc_json_lib_delete(json_parseado);
		return false;
	}
	program->acceleration_up =(uint32_t)atoi(aux_atoi);

	if(false == rsc_json_lib_get_item_data_by_name(json_parseado,  "wait_up", aux_atoi, API_PROGRAM_MAX_INT_TO_STRING)){
		rsc_json_lib_delete(json_parseado);
		return false;
	}
	program->wait_up =(uint32_t)atoi(aux_atoi);

	if(false == rsc_json_lib_get_item_data_by_name(json_parseado,  "depth_sample", aux_atoi, API_PROGRAM_MAX_INT_TO_STRING)){
		rsc_json_lib_delete(json_parseado);
		return false;
	}
	program->depth_sample =(uint32_t)atoi(aux_atoi);

	if(false == rsc_json_lib_get_item_data_by_name(json_parseado,  "displacement_to_sample", aux_atoi, API_PROGRAM_MAX_INT_TO_STRING)){
		rsc_json_lib_delete(json_parseado);
		return false;
	}
	program->displacement_to_sample =(uint32_t)atoi(aux_atoi);

	if(false == rsc_json_lib_get_item_data_by_name(json_parseado,  "loops", aux_atoi, API_PROGRAM_MAX_INT_TO_STRING)){
		rsc_json_lib_delete(json_parseado);
		return false;
	}
	program->loops =(uint32_t)atoi(aux_atoi);


	rsc_json_lib_delete(json_parseado);
	return true;

}


bool user_fs_writing_func_(const char * path , char * buffer, uint32_t buffer_size){

	if(true == board_spiffs_is_mounted()){

    	FILE* f;
        f = board_spiffs_open(path,"w");

        if(NULL == f){
            board_spiffs_close(f);
            ESP_LOGE(TAG, "error opening file");
    		return false;
        }

        board_spiffs_write(f,buffer);
        board_spiffs_write(f,"\n");

        board_spiffs_close(f);

		return true;
	}

	ESP_LOGE(TAG, "error spiffs not mounted ");

	return false;
}

bool user_fs_reading_func_(const char * path , char * buffer, uint32_t buffer_size){

	if(true == board_spiffs_is_mounted()){

	    	FILE* f;
	        f = board_spiffs_open(path,"r");

	        if(NULL == f){
	            board_spiffs_close(f);
	    		return false;
	        }

	        board_spiffs_read_line(f, buffer, buffer_size);

	        board_spiffs_close(f);
			return true;
		}

		return false;
}

bool user_fs_deleting_func_(const char * path ){

	if(true == board_spiffs_is_mounted()){
		return board_spiffs_delete_file(path);
	}

	return false;
}


bool user_fs_renaming_func_(const char * path_old, const char * path_new ){

	if(true == board_spiffs_is_mounted()){
		return board_spiffs_rename_file(path_old, path_new);
	}

	return false;
}



/********************** external functions definition ************************/


bool api_program_manager_write_in_empty(api_program_manager_program_t * p_program){

	if(NULL == p_program){
		return false;
	}

	uint32_t next_empty= 1 + api_program_manager_get_n_programs_in_mem();

	return api_program_manager_write(p_program, next_empty);
}

bool api_program_manager_write(api_program_manager_program_t * p_program, uint8_t index){

	if (API_PROGRAM_MAX_PROGRAM_INDEX < index || (index == 0)){
        return false;
    }

	if(NULL == p_program){
		return false;
	}

    char aux_name_itoa[4]={0};

    user_serialize_func_(p_program, self_.aux_data_buff, API_PROGRAM_DATA_BUFF_SZ);
    ESP_LOGI(TAG,"data serialized: %s",self_.aux_data_buff);

    strcpy( self_.aux_filename_buff, "");
    strcat( self_.aux_filename_buff, API_PROGRAM_FILENAME_BASE);
    itoa(index, aux_name_itoa, 10);
    strcat(self_.aux_filename_buff, aux_name_itoa);
    strcat(self_.aux_filename_buff, ".txt");

    return user_fs_writing_func_(self_.aux_filename_buff, self_.aux_data_buff, API_PROGRAM_DATA_BUFF_SZ);

}

bool  api_program_manager_read(api_program_manager_program_t * program, uint8_t index){

	if (API_PROGRAM_MAX_PROGRAM_INDEX < index || (index == 0)){
        return false;
    }

    char aux_name_itoa[4]={0};

    strcpy(self_.aux_filename_buff, "");
    strcat(self_.aux_filename_buff, API_PROGRAM_FILENAME_BASE);
    itoa(index, aux_name_itoa, 10);
    strcat(self_.aux_filename_buff, aux_name_itoa);
    strcat(self_.aux_filename_buff, ".txt");

    if (user_fs_reading_func_(self_.aux_filename_buff, self_.aux_data_buff, API_PROGRAM_DATA_BUFF_SZ) == true)
    {
    	user_deserialize_func_(program, self_.aux_data_buff, API_PROGRAM_DATA_BUFF_SZ);
        return true;
    }

        return false;
}



bool api_program_manager_delete_and_leftshift(uint8_t index){

	if (API_PROGRAM_MAX_PROGRAM_INDEX < index || (index == 0) ){
		return false;
	}

	//delete
	char aux_name_itoa[4]={0};
	uint32_t programs_in_mem = api_program_manager_get_n_programs_in_mem()-1;

	strcpy(self_.aux_filename_buff, "");
  strcat(self_.aux_filename_buff, API_PROGRAM_FILENAME_BASE);
  itoa(index, aux_name_itoa, 10);
  strcat(self_.aux_filename_buff, aux_name_itoa);
  strcat(self_.aux_filename_buff, ".txt");

	if(false == user_fs_deleting_func_(self_.aux_filename_buff)){
		return false;
	}

	char old_filename_buff[API_PROGRAM_FILENAME_BUFF_SZ];
	char new_filename_buff[API_PROGRAM_FILENAME_BUFF_SZ];


	//shift
	for(int i = index; i<=programs_in_mem; i ++){

		char aux_name_itoa[4]={0};

	    strcpy(old_filename_buff, "");
	    strcat(old_filename_buff, API_PROGRAM_FILENAME_BASE);
	    itoa(i+1, aux_name_itoa, 10);
	    strcat(old_filename_buff, aux_name_itoa);
	    strcat(old_filename_buff, ".txt");

	    strcpy(new_filename_buff, "");
	    strcat(new_filename_buff, API_PROGRAM_FILENAME_BASE);
	    itoa(i, aux_name_itoa, 10);
	    strcat(new_filename_buff, aux_name_itoa);
	    strcat(new_filename_buff, ".txt");

	    if(false == user_fs_renaming_func_(old_filename_buff, new_filename_buff)){
	    	return false;
	    }

	}
    return false;
}


uint32_t api_program_manager_get_n_programs_in_mem(void){

	uint32_t count =0;
	api_program_manager_program_t aux_program;

	for(int i=1; i<API_PROGRAM_MAX_PROGRAM_INDEX; i++){
		if(true ==  api_program_manager_read(&aux_program, i)){
			count++;
		}
		else{
			break;
		}
	}

	return count;
}


void  api_program_manager_init(void){

}
void  api_program_manager_deinit(void){

}
void  api_program_manager_loop(void){

}

/********************** end of file ******************************************/

/** @}Final Doxygen */
