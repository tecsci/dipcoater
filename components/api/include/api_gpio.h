/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		api_gpio.h
* @date		15 jun. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef COMPONENTS_API_INCLUDE_API_GPIO_H_
#define COMPONENTS_API_INCLUDE_API_GPIO_H_
/********************** inclusions *******************************************/

#include "board_gpio.h"

/********************** macros ***********************************************/

/********************** typedef **********************************************/
typedef enum
{
  API_GPIO_PIN_1 = BOARD_GPIO_PIN_1,
  API_GPIO_PIN_2 = BOARD_GPIO_PIN_2,
  API_GPIO_PIN_3 = BOARD_GPIO_PIN_3,
  API_GPIO_PIN_4 = BOARD_GPIO_PIN_4,
  API_GPIO_PIN_5 = BOARD_GPIO_PIN_5,
  API_GPIO_PIN_6 = BOARD_GPIO_PIN_6,
  API_GPIO_PIN_7 = BOARD_GPIO_PIN_7,
  API_GPIO_PIN_8 = BOARD_GPIO_PIN_8,
  API_GPIO_PIN_9 = BOARD_GPIO_PIN_9,
  API_GPIO_PIN_10 = BOARD_GPIO_PIN_10,
  API_GPIO_PIN_11 = BOARD_GPIO_PIN_11,
  API_GPIO_PIN_12 = BOARD_GPIO_PIN_12


}api_gpio_pin_t;

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/
void api_gpio_read(api_gpio_pin_t pin);
void api_gpio_write(api_gpio_pin_t pin, bool state);

void api_gpio_init(void);
void api_gpio_deinit(void);
void api_gpio_loop(void);


#endif /* COMPONENTS_API_INCLUDE_API_GPIO_H_ */
/** @}Final Doxygen */

