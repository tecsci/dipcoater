/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		api_program_manager.h
* @date		4 feb. 2022
* @author
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
*
* @brief
* @{ Init Doxygen
*/

#ifndef API_PROGRAM_MANAGER_H_
#define API_PROGRAM_MANAGER_H_

/********************** inclusions *******************************************/
#include "stdio.h"
#include "stdint.h"
#include "string.h"
#include "stdbool.h"



/********************** macros ***********************************************/


#define API_PROGRAM_NAME_SIZE 		(30)

/********************** typedef **********************************************/

typedef struct{

	/*Program custom for Dip Coater Machine*/

	char name[API_PROGRAM_NAME_SIZE];

	uint32_t velocity_down;
	uint32_t acceleration_down;
	uint32_t wait_down;


	uint32_t velocity_up;
	uint32_t acceleration_up;
	uint32_t wait_up;

	uint32_t displacement_to_sample;
	uint32_t depth_sample;

	uint32_t loops;

}api_program_manager_program_t;

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

bool api_program_manager_write(api_program_manager_program_t * program, uint8_t index);
bool  api_program_manager_read(api_program_manager_program_t * program, uint8_t numberprogram);
bool api_program_manager_delete_and_leftshift(uint8_t index);
uint32_t api_program_manager_get_n_programs_in_mem(void);
bool api_program_manager_write_in_empty(api_program_manager_program_t * program);


void  api_program_manager_init(void);
void  api_program_manager_deinit(void);
void  api_program_manager_loop(void);


#endif /* API_PROGRAM_MANAGER_H_ */
/********************** end of file ******************************************/

/** @}Final Doxygen */
