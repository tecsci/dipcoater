/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* Copyright (C) <2021>  <Martin Abel Gambarotta   magambarotta@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		api_tmc.h
* @date		14 jun. 2021
* @author	
*		-martin
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

#ifndef COMPONENTS_API_INCLUDE_API_TMC_H_
#define COMPONENTS_API_INCLUDE_API_TMC_H_
/********************** inclusions *******************************************/

#include "stdbool.h"
#include "stdint.h"
#include "stddef.h"
/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/
float api_tmc_get_actual_position(void);
void api_tmc_driver_enable(bool state);
void api_tmc_write_read(uint8_t *tx, uint8_t *rx, size_t length);

bool api_tmc_was_stallguard_detected(int32_t readed_rampstat_reg);
bool api_tmc_was_vzero_detected(int32_t readed_rampstat_reg);
bool api_tmc_was_position_reached(int32_t readed_rampstat_reg);
bool api_tmc_is_position_target(int32_t readed_rampstat_reg);
bool api_tmc_was_velocity_reached(int32_t readed_rampstat_reg);

/********************** external functions declaration ***********************/
void api_tmc_init(void);
void api_tmc_deinit(void);
void api_tmc_loop(void);

#endif /* COMPONENTS_API_INCLUDE_API_TMC_H_ */
/** @}Final Doxygen */

