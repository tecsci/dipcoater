#ifndef TMCL_H
#define TMCL_H

#include <stddef.h>
#include "API_Header.h"
#include "defs.h"
#include "TMC_Board.h"
//#include "TMCL.h"
#include "TMC5130.h"

void tmcl_init();
void tmcl_process();
void tmcl_boot();

#endif /* TMCL_H */
