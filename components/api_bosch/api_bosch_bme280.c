/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		api_bme280.c
* @date		23 sep. 2021
* @author	
*		-Martin Abel Gambarotta   <magambarotta@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "api_bosch_bme280.h"

#include "esp_log.h"
#include "board_delay.h"
#include "board_i2c.h"

#include "esp_err.h"
#include "driver/i2c.h"

/********************** macros and definitions *******************************/
/********************** internal data declaration ****************************/
static const char *TAG = "api_bosh_bme280.c";

/********************** internal data definition *****************************/
static struct {

	struct bme280_dev dev;
	struct bme280_data data;

	int8_t rslt;
	uint8_t dev_addr;

} self_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/
static void api_bme280_config_normal_mode_(void){

	uint8_t settings_sel;

	/* Recommended mode of operation: Indoor navigation */
	self_.dev.settings.osr_h = BME280_OVERSAMPLING_1X;
	self_.dev.settings.osr_p = BME280_OVERSAMPLING_16X;
	self_.dev.settings.osr_t = BME280_OVERSAMPLING_2X;
	self_.dev.settings.filter = BME280_FILTER_COEFF_16;
	self_.dev.settings.standby_time = BME280_STANDBY_TIME_62_5_MS;

	settings_sel = BME280_OSR_PRESS_SEL;
	settings_sel |= BME280_OSR_TEMP_SEL;
	settings_sel |= BME280_OSR_HUM_SEL;
	settings_sel |= BME280_STANDBY_SEL;
	settings_sel |= BME280_FILTER_SEL;

	bme280_set_sensor_settings(settings_sel, &self_.dev);
	bme280_set_sensor_mode(BME280_NORMAL_MODE, &self_.dev);

}


int8_t api_bosch_bme280_i2c_write_(uint8_t reg_addr,uint8_t *reg_data, uint32_t len, void *intf_ptr)
{
	int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */
	uint8_t device_addr = *((uint8_t *) intf_ptr);

	rslt = board_i2c_master_write(BOARD_I2C_0,device_addr,reg_addr,reg_data,len);

	return rslt;
}

int8_t api_bosch_bme280_i2c_read_(uint8_t reg_addr, uint8_t *reg_data, uint32_t len, void *intf_ptr)
{
	int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */
	uint8_t device_addr = *((uint8_t *) intf_ptr);

	rslt = board_i2c_master_read(BOARD_I2C_0,device_addr,reg_addr,reg_data,len);


	return rslt;
}


void api_bosch_bme280_delay_(uint32_t period, void *intf_ptr)
{
	board_delay_micros(period);
}


/********************** external functions definition ************************/
void api_bosch_bme280_print_sensor_data_(struct bme280_data *comp_data){
	//ESP_LOGI(TAG,"Temperatura:%0.2f, Presion:%0.2f, Humedad:%0.2f",comp_data->temperature, comp_data->pressure, comp_data->humidity);
}

double api_bosch_bme280_get_temperature(void){
	if( BME280_OK != bme280_get_sensor_data(BME280_ALL, &self_.data, &self_.dev)){
		ESP_LOGE(TAG,"ERROR READING BME280");
		return 0;
	}
	api_bosch_bme280_print_sensor_data_(&self_.data);
	return self_.data.temperature;
}

double api_bosch_bme280_get_pressure(void){
	if( BME280_OK != bme280_get_sensor_data(BME280_ALL, &self_.data, &self_.dev)){
		ESP_LOGE(TAG,"ERROR READING BME280");
		return 0;
	}
	api_bosch_bme280_print_sensor_data_(&self_.data);
	return self_.data.pressure;
}

double api_bosch_bme280_get_humidity(void){
	if( BME280_OK != bme280_get_sensor_data(BME280_ALL, &self_.data, &self_.dev)){
		ESP_LOGE(TAG,"ERROR READING BME280");
		return 0;
	}
	api_bosch_bme280_print_sensor_data_(&self_.data);
	return self_.data.humidity;
}



void api_bosch_bme280_init(){

	//INIT BME 280
	self_.rslt = BME280_OK;
	self_.dev_addr = BME280_I2C_ADDR_PRIM;


	self_.dev.intf_ptr = &self_.dev_addr;
	self_.dev.intf = BME280_I2C_INTF;
	self_.dev.read = api_bosch_bme280_i2c_read_;
	self_.dev.write = api_bosch_bme280_i2c_write_;
	self_.dev.delay_us = api_bosch_bme280_delay_;

	self_.rslt = bme280_init(&self_.dev);

    api_bme280_config_normal_mode_();


}
void api_bosch__bme280_deinit(){}

void api_bosch_bme280_loop() {


#if 0


	if( BME280_OK == bme280_get_sensor_data(BME280_ALL, &self_.data, &self_.dev)){
		api_bosch_bme280_print_sensor_data_(&self_.data);
	}
	else{
		ESP_LOGE(TAG,"ERROR READING BME280");
	}
#endif


}

/********************** end of file ******************************************/

/** @}Final Doxygen */
