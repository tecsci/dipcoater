/* 
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_delay.h
* @date		23 sep. 2021
* @author	
*		-Martin Abel Gambarotta   <magambarotta@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef COMPONENTS_BOARD_INCLUDE_BOARD_DELAY_H_
#define COMPONENTS_BOARD_INCLUDE_BOARD_DELAY_H_
/********************** inclusions *******************************************/
#include "stdint.h"
/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void board_delay_micros(uint32_t micros);
void board_delay_milis(uint32_t milis);
void board_delay_seconds(uint32_t seconds);


void board_delay_init();
void board_delay_deinit();
void board_delay_loop();

#endif /* COMPONENTS_BOARD_INCLUDE_BOARD_DELAY_H_ */
/** @}Final Doxygen */
