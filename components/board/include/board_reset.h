/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_reset.h
* @date		19 oct. 2021
* @author
*		-Martin Abel Gambarotta   <magambarotta@gmail.com>
* @version	v2.0.0
*
* @brief
* @{ Init Doxygen
*/

#ifndef BOARD_RESET_HPP_
#define BOARD_RESET_HPP_

/********************** inclusions *******************************************/
#include "esp_system.h"
/********************** macros ***********************************************/

/********************** typedef **********************************************/

typedef enum{
    BOARD_RESET_UNKNOWN = ESP_RST_UNKNOWN,  	  //!< Reset reason can not be determined
    BOARD_RESET_POWERON = ESP_RST_POWERON,    	//!< Reset due to power-on event
    BOARD_RESET_EXT = ESP_RST_EXT,       		 		//!< Reset by external pin (not applicable for ESP32)
    BOARD_RESET_SW = ESP_RST_SW,         				//!< Software reset via esp_restart
    BOARD_RESET_PANIC = ESP_RST_PANIC,      		//!< Software reset due to exception/panic
    BOARD_RESET_INT_WDT =  ESP_RST_INT_WDT,    	//!< Reset (software or hardware) due to interrupt watchdog
    BOARD_RESET_TASK_WDT = ESP_RST_TASK_WDT,   	//!< Reset due to task watchdog
    BOARD_RESET_WDT = ESP_RST_WDT,        			//!< Reset due to other watchdogs
    BOARD_RESET_DEEPSLEEP = ESP_RST_DEEPSLEEP,  //!< Reset after exiting deep sleep mode
    BOARD_RESET_BROWNOUT = ESP_RST_BROWNOUT,   	//!< Brownout reset (software or hardware)
    BOARD_RESET_SDIO = ESP_RST_SDIO      				//!< Reset over SDIO
}board_reset_reason_t;



/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/


void board_reset_init();
void board_reset_deinit();
void board_reset_loop();

void board_reset_restart(void);
void board_reset_reason_get(board_reset_reason_t* p_reason);

#endif /* RESET_HPP_ */
/********************** end of file ******************************************/
