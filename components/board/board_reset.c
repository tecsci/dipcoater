/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_reset.c
* @date		23 sep. 2021
* @author
*		-Martin Abel Gambarotta   <magambarotta@gmail.com>
* @version	v1.0.0
*
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "board_reset.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static struct {

	board_reset_reason_t reset_reason;

} self_ = { .reset_reason = BOARD_RESET_UNKNOWN };

/********************** external functions definition ************************/

void board_reset_init(void){

	self_.reset_reason  = (board_reset_reason_t) esp_reset_reason();

}
void board_reset_deinit(void) {

}
void board_reset_loop(void) {

}

void board_reset_reason_get(board_reset_reason_t *p_rr) {

	*p_rr = self_.reset_reason;
}

void board_reset_restart(void) {
	esp_restart();
}

/********************** end of file ******************************************/
