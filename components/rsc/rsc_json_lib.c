/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		xsx.c
* @date		1 oct. 2021
* @author
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
*
* @brief
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "rsc_json_lib.h"
#include "stdbool.h"
#include "esp_log.h"

/********************** macros and definitions *******************************/

#define JSON_LIB_PREALLOCATED_STRING_SZ 100

/********************** internal data declaration ****************************/
static const char *TAG = "rsc_json_lib";

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void rsc_json_lib_create(rsc_json_lib_t ** pp_json){
	*pp_json = cJSON_CreateObject();
}

void rsc_json_lib_delete(rsc_json_lib_t * p_json){
	cJSON_Delete(p_json);
}


bool rsc_json_lib_add_data(rsc_json_lib_t * p_json, const char* name, const char* data){

	if(NULL == cJSON_AddStringToObject(p_json, name , data)){
		return false;
	}

	return true;

}

bool rsc_json_lib_add_number(rsc_json_lib_t * p_json, const char* name, double data){

	if(NULL == cJSON_AddNumberToObject(p_json, name , data)){
		return false;
	}

	return true;
}

bool rsc_json_lib_add_bool(rsc_json_lib_t * p_json, const char* name, bool data){

	if(NULL == cJSON_AddBoolToObject(p_json, name , data)){
		return false;
	}

	return true;
}

//le das un objeto de json con los datos agregados y te lo da en un string
bool rsc_json_lib_serialize( rsc_json_lib_t * p_json, char* buff, uint32_t buff_size){

	if( false == cJSON_PrintPreallocated(p_json, buff, buff_size,0)){

		return false;
	}


	return true;

}

//le das un buffer ya serializado y te lo devulve como objeto de json
bool rsc_json_lib_deserialize(rsc_json_lib_t ** pp_json,char * buffer, uint32_t buff_size){

	*pp_json = cJSON_ParseWithLength(buffer,buff_size);

	if(NULL == pp_json){
		return false;
	}

	return true;
}


//cantidad de elementos en un json
uint32_t rsc_json_lib_get_size(rsc_json_lib_t * p_json){

	return (uint32_t) cJSON_GetArraySize(p_json);
}


bool rsc_json_lib_get_item_data_by_name(rsc_json_lib_t * p_json, char * name, char * buffer, uint32_t buff_size ){

	ESP_LOGI(TAG,"%s",__func__);
	//gets
	if (cJSON_GetObjectItem(p_json, name)) {

		if(cJSON_String == cJSON_GetObjectItem(p_json,name)->type){

			char *aux = cJSON_GetObjectItem(p_json,name)->valuestring;
			strncpy(buffer,aux,buff_size);
			return true;

		}
	}

	return false;

}

//el json esta sin deserializar
bool rsc_json_lib_get_n_item_data(rsc_json_lib_t * p_json, uint32_t n, char * buffer, uint32_t buff_size ){


	if(n >= rsc_json_lib_get_size(p_json)){
		return false;
	}

	rsc_json_lib_t * aux_json = cJSON_GetArrayItem(p_json,n);
	if(NULL == aux_json){

		return false;
	}

	return rsc_json_lib_serialize(aux_json, buffer, buff_size);
}

#if 0 //for future version
bool rsc_json_lib_get_n_item_name(rsc_json_lib_t * p_json, uint32_t n, char * buffer, uint32_t buff_size ){

}
#endif

#if 0 //deprecated
bool rsc_json_lib_get_item_data_by_name_num(rsc_json_lib_t * p_json, char * name, int32_t* p_num )
{

	//gets
	if (cJSON_GetObjectItem(p_json, name)) {

		if(cJSON_Number == cJSON_GetObjectItem(p_json,name)->type){

			*p_num = cJSON_GetObjectItem(p_json,name)->valueint;
			return true;

		}


	}
	return false;

}
#endif



/********************** end of file ******************************************/

/** @}Final Doxygen */
