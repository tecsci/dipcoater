/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* Copyright (C) <2021>  <Martin Abel Gambarotta   magambarotta@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		rsc_countdown.h
* @date		14 jun. 2021
* @author	
*		-martin
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

#ifndef COMPONENTS_RSC_INCLUDE_RSC_COUNTDOWN_H_
#define COMPONENTS_RSC_INCLUDE_RSC_COUNTDOWN_H_
/********************** inclusions *******************************************/

/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

#endif /* COMPONENTS_RSC_INCLUDE_RSC_COUNTDOWN_H_ */
/** @}Final Doxygen */

