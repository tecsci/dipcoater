/*
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		mod_hmi_RX.c
* @date		22 sep. 2021
* @author	
*   -Martin Abel Gambarotta   <magambarotta@gmail.com>
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

/******** inclusions ***************/

#include "mod_hmi_RX.h"

#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "api_serial.h"
#include "board_serial.h"
#include "driver/uart.h"
#include "os_config.h"

/*NOTAS DE PANTALLA STONE 27OCT
 *
 * Test con el analizados logico
 * 1)a veces la pantalla manda un solo mensaje en vez de mandar button 1  & button 2     que es el mismo mensaje
 * ") se ve el error en el parseo de la funcion pero la cadena es visualizada en el analizador
 *
 * */


/******** macros and definitions ***********/

#define MOD_HMI_RX_UART_NUM API_UART_1
#define MOD_HMI_RX_VERBOSE true

static struct{

	mod_hmi_RX_command_t command_RX;
	board_serial_uart_event_t event;

} self_;

static const char*TAG ="mod_hmi_RX";

/******** internal data declaration **********/

/******** internal data definition ***********/

const uint8_t header_code_[MOD_HMI_RX_HEADER_SZ]=
												{MOD_HMI_RX_HEADER_B1,
												  MOD_HMI_RX_HEADER_B2,
												  MOD_HMI_RX_HEADER_B3};

const uint8_t tail_code_[MOD_HMI_RX_TAIL_SZ]=
												{MOD_HMI_RX_TAIL_B1,
												  MOD_HMI_RX_TAIL_B2,
												  MOD_HMI_RX_TAIL_B3};

QueueHandle_t mod_hmi_RX_uart_intr_queue = NULL;

/******** external data definition ***********/
QueueHandle_t mod_hmi_RX_queue = NULL;

/******** internal functions definition ********/

static bool read_n_bytes_(uint32_t n , uint8_t* p_buff, bool verbose){

	for(int i =0; i < n; i++){
		if(0 == api_serial_read(MOD_HMI_RX_UART_NUM, &p_buff[i])){
			ESP_LOGI(TAG,"read_n_bytes fail: %d", i);
			return false;
		}

		if( true == verbose){
			ESP_LOGI(TAG,"read_n_bytes %d: 0x%X \n", i, p_buff[i]);
		}
	}

	return true;
}

static bool messages_available_(){

	return api_serial_is_available(MOD_HMI_RX_UART_NUM);
}

static bool header_check_(){

	static uint8_t aux_byte;

	for(int i =0; i< MOD_HMI_RX_HEADER_SZ; i++){

		if(false == read_n_bytes_(1,&aux_byte,MOD_HMI_RX_VERBOSE)){
			ESP_LOGE(TAG, "header fail read");
			return false;
		}

		if(header_code_[i] != aux_byte){
			ESP_LOGE(TAG, "header parse false %c",(char)aux_byte);
			return false;
		}
	}
	return true;

}

static bool tail_check_(){

	static uint8_t aux_byte;

	for(int i =0; i< MOD_HMI_RX_TAIL_SZ; i++){

		if(false == read_n_bytes_(1,&aux_byte,MOD_HMI_RX_VERBOSE)){
			ESP_LOGE(TAG, "tail fail read");
			return false;
		}

		if(tail_code_[i] != aux_byte){
			ESP_LOGE(TAG, "tail parse false");
			return false;
		}

	}
	return true;

}

//TODO Check Calculation CRC
static bool crc_check_(){

	static uint8_t aux_bytes[2];

	//READ 3 BYTES
	if(false == read_n_bytes_(2,aux_bytes,MOD_HMI_RX_VERBOSE)){
		ESP_LOGI(TAG, "crc fail read");
		return false;
	}

	return true;
}

static bool parse_command_code_(uint32_t* p_buff){

	uint8_t aux_bytes[MOD_HMI_RX_CMD_CODE_SZ] = {0};

	if(true == read_n_bytes_(MOD_HMI_RX_CMD_CODE_SZ, aux_bytes,MOD_HMI_RX_VERBOSE)){

		*p_buff = (aux_bytes[0] << 8) | (aux_bytes[1]);

		return true;

	}

	return false;

}


static bool parse_data_len_(uint32_t* p_buff){

	uint8_t aux_bytes[2] = {0,0};

	if(true == read_n_bytes_(2, aux_bytes,MOD_HMI_RX_VERBOSE)){

		*p_buff = (aux_bytes[0]) << 8 |  (aux_bytes[1]);

		return true;

	}

	return false;

}

#if 0
static void event_analisis_(uart_event_t event){
  size_t buffered_size;
  ESP_LOGI(TAG, "uart[%d] event:", BOARD_UART_1);

  switch(event.type) {
      //Event of UART receving data
      /*We'd better handler data event fast, there would be much more data events than
      other types of events. If we take too much time on data event, the queue might
      be full.*/
      case UART_DATA:
          ESP_LOGI(TAG, "UART DATA event: %d", event.size);
#if 0
          uart_read_bytes(BOARD_UART_1, dtmp, event.size, portMAX_DELAY);
          ESP_LOGI(TAG, "[DATA EVT]:");
          uart_write_bytes(BOARD_UART_1, (const char*) dtmp, event.size);
#endif
          break;
      //Event of HW FIFO overflow detected
      case UART_FIFO_OVF:
          ESP_LOGI(TAG, "hw fifo overflow");
          // If fifo overflow happened, you should consider adding flow control for your application.
          // The ISR has already reset the rx FIFO,
          // As an example, we directly flush the rx buffer here in order to read more data.
          uart_flush_input(BOARD_UART_1);
          xQueueReset(mod_hmi_RX_uart_intr_queue);
          break;
      //Event of UART ring buffer full
      case UART_BUFFER_FULL:
          ESP_LOGI(TAG, "ring buffer full");
          // If buffer full happened, you should consider encreasing your buffer size
          // As an example, we directly flush the rx buffer here in order to read more data.
          uart_flush_input(BOARD_UART_1);
          xQueueReset(mod_hmi_RX_uart_intr_queue);
          break;
      //Event of UART RX break detected
      case UART_BREAK:
          ESP_LOGI(TAG, "uart rx break");
          break;
      //Event of UART parity check error
      case UART_PARITY_ERR:
          ESP_LOGI(TAG, "uart parity error");
          break;
      //Event of UART frame error
      case UART_FRAME_ERR:
          ESP_LOGI(TAG, "uart frame error");
         break;
      //UART_PATTERN_DET
      case UART_PATTERN_DET:
      	uart_get_buffered_data_len(BOARD_UART_1, &buffered_size);
          int pos = uart_pattern_pop_pos(BOARD_UART_1);
          ESP_LOGI(TAG, "UART PATTERN DETECTED \n pos: %d, buffered size: %d", pos, buffered_size);
          break;
#if 0
          if (pos == -1) {
              // There used to be a UART_PATTERN_DET event, but the pattern position queue is full so that it can not
              // record the position. We should set a larger queue size.
              // As an example, we directly flush the rx buffer here.
              uart_flush_input(EX_UART_NUM);
          } else {
#if 0
              uart_read_bytes(EX_UART_NUM, dtmp, pos, 100 / portTICK_PERIOD_MS);
              uint8_t pat[PATTERN_CHR_NUM + 1];
              memset(pat, 0, sizeof(pat));
              uart_read_bytes(EX_UART_NUM, pat, PATTERN_CHR_NUM, 100 / portTICK_PERIOD_MS);
              ESP_LOGI(TAG, "read data: %s", dtmp);
              ESP_LOGI(TAG, "read pat : %s", pat);
#endif

	//READ MSG DATA
	if( true == read_n_bytes_(buffered_size,buff_,true)){
		//mensaje leido ok
	}


          }
#endif
          break;
      //Others
      default:
          ESP_LOGI(TAG, "uart event type: %d", event.type);
          break;
  }
  return;

}

#endif

/******** external functions definition ********/
void mod_hmi_RX_init(void) {


	board_serial_get_intr_queue(BOARD_UART_1, &mod_hmi_RX_uart_intr_queue);
	mod_hmi_RX_queue = xQueueCreate( OS_CONFIG_MOD_HMI_RX_QUEUE_SIZE , sizeof( mod_hmi_RX_command_t ) );

	//task create
    xTaskCreate(
    		mod_hmi_RX_task_loop
      ,  "hmi RX task"   // A name just for humans
      ,  OS_CONFIG_MOD_HMI_RX_TASK_SIZE  // This stack size can be checked & adjusted by reading the Stack Highwater
      ,  NULL
      ,  OS_CONFIG_MOD_HMI_RX_TASK_PRIORITY  // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
      ,  NULL
      );

}

void mod_hmi_RX_deinit(void) {


}

/*
 * COMMANDS: HEADER + CMD_CODE + DATA_LEN + DATA + TAIL + CRC
 */
void mod_hmi_RX_task_loop(void *argument) {

#if 0
	static board_serial_uart_event_t event;
	//INIT


	board_serial_get_intr_queue(BOARD_UART_1, &mod_hmi_RX_uart_intr_queue);
	mod_hmi_RX_queue = xQueueCreate( OS_CONFIG_MOD_HMI_RX_QUEUE_SIZE , sizeof( mod_hmi_RX_command_t ) );

#endif


#if 1
	   if( ( mod_hmi_RX_queue == NULL ) ){
			for(;;){
			   	  ESP_LOGE(TAG, "mod_hmi_RX_task_loop: error creating mod_hmi_RX_queue");
			   	  vTaskDelay(1000/ portTICK_PERIOD_MS);
			}
		}
#endif

	//LOOP
	for(;;){

	   if( ( mod_hmi_RX_uart_intr_queue != NULL ) ){

				if(xQueueReceive(mod_hmi_RX_uart_intr_queue, (void * )&self_.event, (portTickType)portMAX_DELAY)) {

					//CHECK COMM
					if (true == board_serial_event_is_rxdata(self_.event)) {

						while(true == messages_available_()){
	#if true == MOD_HMI_RX_VERBOSE
							ESP_LOGI(TAG,"msg available");
	#endif

							//HEADER CHECK STATE
							if(true == header_check_() ){

								//ESP_LOGI(TAG,"header ok\n");

								//PARSE COMMAND

								//READ COMMAND CODE
								if(true == parse_command_code_(&self_.command_RX.command_code)){

									//ESP_LOGI(TAG, "command code: 0x%X\n", self_.command_RX.command_code);

									//READ DATA LEN
									if(true == parse_data_len_(&self_.command_RX.len)){

										//ESP_LOGI(TAG, "data len %d:",self_.command_RX.len);

										//READ MSG DATA
										if( true == read_n_bytes_(self_.command_RX.len,self_.command_RX.data,MOD_HMI_RX_VERBOSE)){
											//mensaje leido ok

											if( true == tail_check_() ){

												if( true == crc_check_() ){

													if(NULL != mod_hmi_RX_queue ){
														xQueueGenericSend(mod_hmi_RX_queue, &self_.command_RX,0U, 0U);
													}

													//clear data
													self_.command_RX.command_code=0;
													self_.command_RX.len=0;
													memset(self_.command_RX.data, 0, MOD_HMI_RX_DATA_MAX_SZ);
												}
											}
										}
									}
								}
							}
							vTaskDelay(OS_CONFIG_MOD_HMI_RX_TASK_PERIOD / portTICK_PERIOD_MS);
						}

				 		vTaskDelay(OS_CONFIG_MOD_HMI_RX_TASK_PERIOD / portTICK_PERIOD_MS);
					}

				}
	   }
	   else{
	  	 ESP_LOGE(TAG, "uart intr queue is NULL");
	 		vTaskDelay(100 / portTICK_PERIOD_MS);
	   }

	}

}

/******** end of file **************/

/** @}Final Doxygen */
