/* 
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		mod_hmi_RX.h
* @date		22 sep. 2021
* @author	
*   -Martin Abel Gambarotta   magambarotta@gmail.com
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef _MOD_HMI_RX_H_
#define _MOD_HMI_RX_H_

/******** inclusions ***************/

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "stdbool.h"
#include "stdint.h"

/******** macros *****************/

#define MOD_HMI_RX_HEADER_SZ 3
#define MOD_HMI_RX_HEADER_B1 0x53
#define MOD_HMI_RX_HEADER_B2 0x54
#define MOD_HMI_RX_HEADER_B3 0x3C

#define MOD_HMI_RX_TAIL_SZ 3
#define MOD_HMI_RX_TAIL_B1 0x3E
#define MOD_HMI_RX_TAIL_B2 0x45
#define MOD_HMI_RX_TAIL_B3 0x54

#define MOD_HMI_RX_CMD_CODE_SZ 2

#define MOD_HMI_RX_LEN_SZ 2

#define MOD_HMI_RX_DATA_MAX_SZ 32

/******** typedef ****************/

typedef struct{

	uint32_t command_code;
	uint32_t len;
	uint8_t data[MOD_HMI_RX_DATA_MAX_SZ]; //podria ser dinamico

}mod_hmi_RX_command_t;

/******** external data declaration **********/

extern QueueHandle_t mod_hmi_RX_queue;

/******** external functions declaration *********/

void mod_hmi_RX_init();
void mod_hmi_RX_deinit();
void mod_hmi_RX_task_loop();

#endif /* _MOD_HMI_RX_H_ */
/** @}Final Doxygen */
