/*
* <TECSCI Dip Coater     Technology for Science   info@tecsci.com.ar>
* Copyright (C) <2021>  <Martin Abel Gambarotta   magambarotta@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		app_hmi.c
* @date		12 Oct. 2021
* @author	
*		-Martin Abel Gambarotta   magambarotta@gmail.com
*		-Lucas Mancini						mancinilucas95@gmail.com
* @version	v1.0.2
* @brief
*
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/

#include "app_hmi.h"
#include "driver/uart.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "math.h"

#include "api.h"
#include "board.h"
#include "mod_hmi_RX.h"
#include "rsc_statemachine.h"

#include "app_coating.h"
#include "mod_coating_process.h"
#include "mod_coating_handlers.h"
#include "os_config.h"
#include "machine.h"

/********************** macros and definitions *******************************/

#define APP_HMI_WIDGET_NAME_SZ 512
#define APP_HMI_WIDGET_EDITTEXT_DATA_SIZE 50
#define APP_HMI_RUNNING_STATUS_STR_SIZE API_PROGRAM_NAME_SIZE

#define APP_HMI_N_PROGRAMS_PER_PAGE 4

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

typedef struct{

	char name[APP_HMI_WIDGET_NAME_SZ];
	void (*handler)(void);

}app_hmi_widget_t;

/*WIDGETS ENUM*/
typedef enum{

	//Test
	APP_HMI_WIDGET_TEST=0,

	//Menu
	APP_HMI_WD_MENU_TO_MANUAL,
	APP_HMI_WD_MENU_TO_RUNNING,
	APP_HMI_WD_MENU_TO_PROGRAMS,
	APP_HMI_WD_MENU_TO_NEW,
	APP_HMI_WD_MENU_TO_SETTINGS,

	//Manual
	APP_HMI_WD_MANUAL_TO_MENU,
	APP_HMI_WD_MANUAL_DZ1,
	APP_HMI_WD_MANUAL_DZ2,
	APP_HMI_WD_MANUAL_DZ3,
	APP_HMI_WD_MANUAL_ZERO,
	APP_HMI_WD_MANUAL_UP,
	APP_HMI_WD_MANUAL_DOWN,

	//Protocol Running
	APP_HMI_WD_RUNNING_TO_MENU,
	APP_HMI_WD_RUNNING_STOP,

	//Saved Programs
	APP_HMI_WD_PROGRAMS_TO_NEW,
	APP_HMI_WD_NEXT_PAGE,
	APP_HMI_WD_PREV_PAGE,
	APP_HMI_WD_PROGRAMS_TO_MENU,
	APP_HMI_WD_SLOT1_SELECTED,
	APP_HMI_WD_SLOT2_SELECTED,
	APP_HMI_WD_SLOT3_SELECTED,
	APP_HMI_WD_SLOT4_SELECTED,
	APP_HMI_WD_SLOT5_SELECTED,

	//New Program
	APP_HMI_WD_NEW_TO_MENU,
	APP_HMI_WD_PLAY_NEW,
	APP_HMI_WD_SAVE_NEW,
	APP_HMI_WD_NEW_NAME,
	APP_HMI_WD_NEW_VEL_DOWN,
	APP_HMI_WD_NEW_ACCEL_DOWN,
	APP_HMI_WD_NEW_WAIT_DOWN,
	APP_HMI_WD_NEW_VEL_UP,
	APP_HMI_WD_NEW_ACCEL_UP,
	APP_HMI_WD_NEW_WAIT_UP,
	APP_HMI_WD_NEW_DISPLACEMENT_TO_SAMPLE,
	APP_HMI_WD_NEW_DEPTH_SAMPLE,
	APP_HMI_WD_NEW_LOOPS,
	APP_HMI_WD_NEW_TO_POSITION,

	//Selected
	APP_HMI_WD_PLAY_SELECTED,
	APP_HMI_WD_DELETE_SELECTED,
	APP_HMI_WD_SELECTED_TO_MENU,

	//Settings
	APP_HMI_WD_SETTINGS_TO_MENU,
	APP_HMI_WD_SETTINGS_RESET_MACHINE,
	APP_HMI_WD_SETTINGS_ORIGIN,
	APP_HMI_WD_SETTINGS_BOTTOM_LIMIT,
	APP_HMI_WD_SETTINGS_TO_INFO,

	//Info
	APP_HMI_WD_INFO_TO_SETTINGS,

	//Position
	APP_HMI_WD_POSITION_TO_NEW,
	APP_HMI_WD_POSITION_DZ1,
	APP_HMI_WD_POSITION_DZ2,
	APP_HMI_WD_POSITION_DZ3,
	APP_HMI_WD_POSITION_ZERO,
	APP_HMI_WD_POSITION_UP,
	APP_HMI_WD_POSITION_DOWN,

	//Bottom Limit
	APP_HMI_WD_BOTTOM_TO_SETTINGS,
	APP_HMI_WD_BOTTOM_DZ1,
	APP_HMI_WD_BOTTOM_DZ2,
	APP_HMI_WD_BOTTOM_DZ3,
	APP_HMI_WD_BOTTOM_ZERO,
	APP_HMI_WD_BOTTOM_UP,
	APP_HMI_WD_BOTTOM_DOWN,
	APP_HMI_WD_BOTTOM_OK,
	APP_HMI_WD_BOTTOM_DEFAULT_VAL,

	//Pop Up
	APP_HMI_WD_POPUP_SAVED_OK,

	APP_HMI_WIDGET_TAG__N
}app_hmi_widget_tags_t;


static struct{

	//STATE MACHINE
	rsc_statemachine_t statemachine;

	//WIDGET DATA
	app_hmi_widget_t widgets[APP_HMI_WIDGET_TAG__N];
	char wd_name[APP_HMI_WIDGET_NAME_SZ];
	char wd_text[APP_HMI_WIDGET_EDITTEXT_DATA_SIZE];
	int32_t wd_int;
	api_stone_rx_button_val_t wd_bt;

	//RX DATA
	mod_hmi_RX_command_t command_RX;

}self_;


static struct{
	uint8_t dz;
}window_manual_vars_;

static struct{
	uint8_t dz;
}window_position_vars_;

static struct{
	float actual_limit;
	uint8_t dz;
}window_bottom_vars_;

static struct{
	bool dumy;
}window_setting_vars_;

static struct{
	bool dumy;
}window_info_vars_;

static struct{
	uint8_t actual_page;
	bool next_page;
	bool previous_page;

}window_programs_vars_;

static struct{
	uint8_t selected;
	uint8_t triger_slotn;
	bool play;
	bool delete;
}window_selected_program_vars_;

static struct{
	bool play;
	bool save;
	api_program_manager_program_t program;

}window_new_program_vars_;

static struct{
	bool dumy;

}window_running_program_vars_;

static struct{
	bool dumy;

}window_popupsaved_vars_;

static const char *TAG = "app_hmi";
/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/*STATE MACHINE*/
static void state_window_menu_(rsc_statemachine_t *sm);
static void state_window_manual_(rsc_statemachine_t *sm);
static void state_window_settings_(rsc_statemachine_t *sm);
static void state_window_info_(rsc_statemachine_t *sm);
static void state_window_programs_(rsc_statemachine_t *sm);
static void state_window_selected_program_(rsc_statemachine_t *sm);
static void state_window_new_program_(rsc_statemachine_t *sm);
static void state_window_running_program_(rsc_statemachine_t *sm);
static void state_window_position_(rsc_statemachine_t *sm);
static void state_window_bottom_limit_(rsc_statemachine_t *sm);
static void state_window_popup_saved_(rsc_statemachine_t *sm);

/*WIDGET ACTION CONFIG*/
static void widget_actions_config_(void) {

	//TEST WIDGETS
	strncpy(self_.widgets[APP_HMI_WIDGET_TEST].name,
			"test", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WIDGET_TEST].handler=widget_test_action_handler_;

	//MENU WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_MENU_TO_PROGRAMS].name,
			"menu_to_programs", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MENU_TO_PROGRAMS].handler=widget_menu_to_programs_handler_;

	strncpy(self_.widgets[APP_HMI_WD_MENU_TO_MANUAL].name,
			"menu_to_manual", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MENU_TO_MANUAL].handler=widget_menu_to_manual_handler_;

	strncpy(self_.widgets[APP_HMI_WD_MENU_TO_RUNNING].name,
			"menu_to_running", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MENU_TO_RUNNING].handler=widget_menu_to_running_handler_;

	strncpy(self_.widgets[APP_HMI_WD_MENU_TO_SETTINGS].name,
			"menu_to_settings", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MENU_TO_SETTINGS].handler=widget_menu_to_settings_handler_;

	strncpy(self_.widgets[APP_HMI_WD_MENU_TO_NEW].name,
			"menu_to_new", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MENU_TO_NEW].handler=widget_menu_to_new_handler_;


	//PROGRAMS WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_PROGRAMS_TO_MENU].name,
			"programs_to_menu", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_PROGRAMS_TO_MENU].handler=widget_programs_to_menu_handler_;

	strncpy(self_.widgets[APP_HMI_WD_NEXT_PAGE].name,
			"programs_next_pag", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEXT_PAGE].handler=widget_next_page_action_handler_;

	strncpy(self_.widgets[APP_HMI_WD_PREV_PAGE].name,
			"programs_prev_pag", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_PREV_PAGE].handler=widget_prev_page_action_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SLOT1_SELECTED].name,
			"p_slot1_selected", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SLOT1_SELECTED].handler=widget_slot1_selected_handler_;

		strncpy(self_.widgets[APP_HMI_WD_SLOT2_SELECTED].name,
			"p_slot2_selected", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SLOT2_SELECTED].handler=widget_slot2_selected_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SLOT3_SELECTED].name,
			"p_slot3_selected", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SLOT3_SELECTED].handler=widget_slot3_selected_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SLOT4_SELECTED].name,
			"p_slot4_selected", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SLOT4_SELECTED].handler=widget_slot4_selected_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SLOT5_SELECTED].name,
			"p_slot5_selected", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SLOT5_SELECTED].handler=widget_slot5_selected_handler_;

	strncpy(self_.widgets[APP_HMI_WD_PROGRAMS_TO_NEW].name,
			"programs_to_new", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_PROGRAMS_TO_NEW].handler=widget_programs_to_new_handler_;

	//SELECTED WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_PLAY_SELECTED].name,
			"selected_play", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_PLAY_SELECTED].handler=widget_play_selected_handler_;

	strncpy(self_.widgets[APP_HMI_WD_DELETE_SELECTED].name,
			"selected_delete", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_DELETE_SELECTED].handler=widget_delete_selected_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SELECTED_TO_MENU].name,
			"selected_to_menu", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SELECTED_TO_MENU].handler=widget_selected_to_menu_handler_;

	//RUNNING WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_RUNNING_TO_MENU].name,
			"running_to_menu", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_RUNNING_TO_MENU].handler=widget_running_to_menu_handler_;

	strncpy(self_.widgets[APP_HMI_WD_RUNNING_STOP].name,
			"running_stop", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_RUNNING_STOP].handler=widget_running_stop_handler_;

	//NEW WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_NEW_TO_MENU].name,
			"new_to_menu", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEW_TO_MENU].handler=widget_new_to_menu_handler_;

	strncpy(self_.widgets[APP_HMI_WD_PLAY_NEW].name,
			"new_play", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_PLAY_NEW].handler=widget_new_play_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SAVE_NEW].name,
			"new_save", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SAVE_NEW].handler=widget_new_save_handler_;

	strncpy(self_.widgets[APP_HMI_WD_NEW_NAME].name,
			"new_name", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEW_NAME].handler=widget_new_name_handler_;

	strncpy(self_.widgets[APP_HMI_WD_NEW_VEL_DOWN].name,
			"new_vel_down", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEW_VEL_DOWN].handler=widget_new_vel_down_handler_;

	strncpy(self_.widgets[APP_HMI_WD_NEW_VEL_UP].name,
			"new_vel_up", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEW_VEL_UP].handler=widget_new_vel_up_handler_;

	strncpy(self_.widgets[APP_HMI_WD_NEW_ACCEL_DOWN].name,
			"new_accel_down", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEW_ACCEL_DOWN].handler=widget_new_accel_down_handler_;

	strncpy(self_.widgets[APP_HMI_WD_NEW_ACCEL_UP].name,
			"new_accel_up", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEW_ACCEL_UP].handler=widget_new_accel_up_handler_;

	strncpy(self_.widgets[APP_HMI_WD_NEW_WAIT_DOWN].name,
			"new_wait_down", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEW_WAIT_DOWN].handler=widget_new_wait_down_handler_;

	strncpy(self_.widgets[APP_HMI_WD_NEW_WAIT_UP].name,
			"new_wait_up", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEW_WAIT_UP].handler=widget_new_wait_up_handler_;

	strncpy(self_.widgets[APP_HMI_WD_NEW_DISPLACEMENT_TO_SAMPLE].name,
			"new_disp_sample", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEW_DISPLACEMENT_TO_SAMPLE].handler=widget_new_disp_to_sample_handler_;

	strncpy(self_.widgets[APP_HMI_WD_NEW_DEPTH_SAMPLE].name,
			"new_depth_sample", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEW_DEPTH_SAMPLE].handler=widget_new_depth_sample_handler_;

	strncpy(self_.widgets[APP_HMI_WD_NEW_LOOPS].name,
			"new_loops", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEW_LOOPS].handler=widget_new_loops_handler_;

	strncpy(self_.widgets[APP_HMI_WD_NEW_TO_POSITION].name,
			"new_to_pos", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_NEW_TO_POSITION].handler=widget_new_to_pos_handler_;

	//INFO WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_INFO_TO_SETTINGS].name,
			"info_to_menu", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_INFO_TO_SETTINGS].handler=widget_info_to_settings_handler_;

	//SETTINGS WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_SETTINGS_TO_MENU].name,
			"settings_to_menu", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SETTINGS_TO_MENU].handler=widget_settings_to_menu_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SETTINGS_RESET_MACHINE].name,
			"settings_reset_m", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SETTINGS_RESET_MACHINE].handler=widget_settings_reset_m_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SETTINGS_ORIGIN].name,
			"settings_origin", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SETTINGS_ORIGIN].handler=widget_settings_origin_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SETTINGS_BOTTOM_LIMIT].name,
			"settings_bottom", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SETTINGS_BOTTOM_LIMIT].handler=widget_settings_bottom_handler_;

	strncpy(self_.widgets[APP_HMI_WD_SETTINGS_TO_INFO].name,
			"settings_to_info", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_SETTINGS_TO_INFO].handler=widget_settings_to_info_handler_;

	//MANUAL WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_MANUAL_TO_MENU].name,
			"manual_to_menu", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MANUAL_TO_MENU].handler=widget_manual_to_menu_handler_;

	strncpy(self_.widgets[APP_HMI_WD_MANUAL_DZ1].name,
			"manual_dz1", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MANUAL_DZ1].handler=widget_manual_dz1_handler_;

	strncpy(self_.widgets[APP_HMI_WD_MANUAL_DZ2].name,
			"manual_dz2", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MANUAL_DZ2].handler=widget_manual_dz2_handler_;

	strncpy(self_.widgets[APP_HMI_WD_MANUAL_DZ3].name,
			"manual_dz3", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MANUAL_DZ3].handler=widget_manual_dz3_handler_;

	strncpy(self_.widgets[APP_HMI_WD_MANUAL_ZERO].name,
			"manual_zero", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MANUAL_ZERO].handler= widget_settings_origin_handler_;//widget_manual_zero_handler_;

	strncpy(self_.widgets[APP_HMI_WD_MANUAL_UP].name,
			"manual_up", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MANUAL_UP].handler=widget_manual_up_handler_;

	strncpy(self_.widgets[APP_HMI_WD_MANUAL_DOWN].name,
			"manual_down", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_MANUAL_DOWN].handler=widget_manual_down_handler_;

	//POSITION WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_POSITION_TO_NEW].name,
			"position_to_new", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_POSITION_TO_NEW].handler=widget_position_to_new_handler_;

	strncpy(self_.widgets[APP_HMI_WD_POSITION_DZ1].name,
			"position_dz1", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_POSITION_DZ1].handler=widget_position_dz1_handler_;

	strncpy(self_.widgets[APP_HMI_WD_POSITION_DZ2].name,
			"position_dz2", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_POSITION_DZ2].handler=widget_position_dz2_handler_;

	strncpy(self_.widgets[APP_HMI_WD_POSITION_DZ3].name,
			"position_dz3", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_POSITION_DZ3].handler=widget_position_dz3_handler_;

	strncpy(self_.widgets[APP_HMI_WD_POSITION_UP].name,
			"position_up", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_POSITION_UP].handler=widget_position_up_handler_;

	strncpy(self_.widgets[APP_HMI_WD_POSITION_DOWN].name,
			"position_down", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_POSITION_DOWN].handler=widget_position_down_handler_;

	//BOTTOM LIMIT WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_BOTTOM_TO_SETTINGS].name,
			"bottom_to_settings", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_BOTTOM_TO_SETTINGS].handler=widget_bottom_to_settings_handler_;

	strncpy(self_.widgets[APP_HMI_WD_BOTTOM_DZ1].name,
			"bottom_dz1", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_BOTTOM_DZ1].handler=widget_bottom_dz1_handler_;

	strncpy(self_.widgets[APP_HMI_WD_BOTTOM_DZ2].name,
			"bottom_dz2", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_BOTTOM_DZ2].handler=widget_bottom_dz2_handler_;

	strncpy(self_.widgets[APP_HMI_WD_BOTTOM_DZ3].name,
			"bottom_dz3", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_BOTTOM_DZ3].handler=widget_bottom_dz3_handler_;

	strncpy(self_.widgets[APP_HMI_WD_BOTTOM_UP].name,
			"bottom_up", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_BOTTOM_UP].handler=widget_bottom_up_handler_;

	strncpy(self_.widgets[APP_HMI_WD_BOTTOM_DOWN].name,
			"bottom_down", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_BOTTOM_DOWN].handler=widget_bottom_down_handler_;

	strncpy(self_.widgets[APP_HMI_WD_BOTTOM_OK].name,
			"bottom_ok", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_BOTTOM_OK].handler=widget_bottom_limit_ok_handler_;

	strncpy(self_.widgets[APP_HMI_WD_BOTTOM_DEFAULT_VAL].name,
			"bottom_default", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_BOTTOM_DEFAULT_VAL].handler=widget_bottom_default_handler_;

	//POPUP SAVED WIDGETS
	strncpy(self_.widgets[APP_HMI_WD_POPUP_SAVED_OK].name,
			"popupsaved_ok", APP_HMI_WIDGET_NAME_SZ);
	self_.widgets[APP_HMI_WD_POPUP_SAVED_OK].handler=widget_popupsaved_ok_handler_;

}

//COMMAND PARSING
static void app_hmi_widget_parsing_(void)
{
	bool status_ok = false;
    //Loop
    if( mod_hmi_RX_queue != NULL )
 	   {
 	      if( xQueueReceive( mod_hmi_RX_queue, &self_.command_RX , 0 ) == pdPASS )
 	      {
#if 0
 	    	  ESP_LOGI(TAG, "command code: %d", self_.command_RX.command_code);
 	    	  ESP_LOGI(TAG, "len: %d", self_.command_RX.len);
#endif
 	        switch((api_stone_rx_code_t)  self_.command_RX.command_code ){
 	        	case API_STONE_RX_CODE_BUTTON:
 	        		status_ok=api_stone_rx_process_button(self_.wd_name, &self_.wd_bt, self_.command_RX.data, self_.command_RX.len);
 	        		ESP_LOGI(TAG, "button: %d",self_.wd_bt);
 	        		break;

 	        	case API_STONE_RX_CODE_EDIT_TEXT:
 	        		ESP_LOGI(TAG, "edit text");
 	        		status_ok =  api_stone_rx_process_edit_text(self_.wd_name, self_.wd_text, self_.command_RX.data, self_.command_RX.len);
 	        		break;

 	        	case API_STONE_RX_CODE_EDIT_INT:
 	        		ESP_LOGI(TAG, "edit int");
 	        		status_ok= api_stone_rx_process_edit_int(self_.wd_name, &self_.wd_int, self_.command_RX.data, self_.command_RX.len);
 	        		break;

 	        	case API_STONE_RX_CODE_LABEL_TEXT:
 	        		ESP_LOGI(TAG, "label int");
 	        		status_ok= api_stone_rx_process_edit_int(self_.wd_name, &self_.wd_int, self_.command_RX.data, self_.command_RX.len) ;
 	        		break;
 	        	default:
 	        		break;
 	        }


 	        if(true == status_ok){
 	        	//ESP_LOGI(TAG, "name: %s", self_.wd_name);
						for(uint8_t i=0; i<APP_HMI_WIDGET_TAG__N ; i++){

							if(0 == strcmp(self_.wd_name, self_.widgets[i].name)){
								 self_.widgets[i].handler();
							}
						}
 	        }
 	        else{
 	        	//ESP_LOGE(TAG, "cmd parsing - error procesing ");
 	        	status_ok=true;
 	        }

 	      }
 	   }
}

static bool app_hmi_write_slot_name_(uint32_t slot_num, char* str, uint32_t data_len){

	static char aux_widget_name[15];
	static char aux_itoa[4];

	itoa(slot_num, aux_itoa, 10);
	strcpy(aux_widget_name, "slot_");
    strcat(aux_widget_name, aux_itoa);

	return api_stone_tx_set_text(aux_widget_name, API_STONE_SET_TEXT_LABEL, str, data_len);
}

static bool app_hmi_write_selected_program_(api_program_manager_program_t program){

	if(false == api_stone_tx_set_text("selected_name", API_STONE_SET_TEXT_LABEL, program.name, strlen(program.name))){
		return false;
	}
	api_stone_tx_set_int("selected_vel_down", API_STONE_SET_TEXT_EDIT, program.velocity_down);
	api_stone_tx_set_int("selected_accel_down", API_STONE_SET_TEXT_EDIT, program.acceleration_down);
	api_stone_tx_set_int("selected_wait_down", API_STONE_SET_TEXT_EDIT, program.wait_down);

	api_stone_tx_set_int("selected_vel_up", API_STONE_SET_TEXT_EDIT, program.velocity_up);
	api_stone_tx_set_int("selected_accel_up", API_STONE_SET_TEXT_EDIT, program.acceleration_up);
	api_stone_tx_set_int("selected_wait_up", API_STONE_SET_TEXT_EDIT, program.wait_up);

	api_stone_tx_set_float("selected_disp_sample", API_STONE_SET_TEXT_EDIT, program.displacement_to_sample);
	api_stone_tx_set_int("selected_depth_sample", API_STONE_SET_TEXT_EDIT, program.depth_sample);
	api_stone_tx_set_int("selected_loops", API_STONE_SET_TEXT_EDIT, program.loops);

	return true;

}

static void app_hmi_run_program_(api_program_manager_program_t aux_program){
	ESP_LOGI(TAG,"funcion to run program: %s", aux_program.name);
	processCommand_t aux_process_comand;


	//Program
	if (xQueueSend(xQueueCompleteProgram, &aux_program, (TickType_t ) 0) != pdPASS){
		ESP_LOGE(TAG,"Command deleted, errQUEUE");
	  }
	//Command

	aux_process_comand.commandnumber = PROCESS_COMMAND_RUN;
	if (xQueueSend(xQueueIndividualCommand, &aux_process_comand, (TickType_t) 0) != pdPASS) {
			ESP_LOGE(TAG,"Command deleted, errQUEUE");
	}

}

static bool app_hmi_save_program_(api_program_manager_program_t* p_program){

	ESP_LOGI(TAG,"funcion to run program: %s", p_program->name);
	return api_program_manager_write_in_empty(p_program);

}

static void app_hmi_delete_program_(uint8_t index){
	api_program_manager_delete_and_leftshift(index);
}

static void app_hmi_reset_machine_(void){
	processCommand_t aux_process_comand;
	ESP_LOGI(TAG,"%s",__func__);
	aux_process_comand.commandnumber = PROCESS_COMMAND_RESET;
	if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,(TickType_t ) 0) != pdPASS) {
		ESP_LOGE(TAG,"Command deleted, errQUEUE");
	}
}

static void app_hmi_set_machine_info_(void){

#if 0
	char mac[7];
   	board_mac_get_default((uint8_t*)mac);
   	mac[6]='\0';
    api_stone_tx_set_text("info_machineid", API_STONE_SET_TEXT_LABEL, mac, strlen(mac));
#endif

   	api_stone_tx_set_text("info_machineid", API_STONE_SET_TEXT_LABEL, MACHINE_INFO_ID,strlen(MACHINE_INFO_ID));
    api_stone_tx_set_int("info_firmwv", API_STONE_SET_TEXT_LABEL, MACHINE_INFO_FIRMWARE_VERSION);
    api_stone_tx_set_int("info_hardv", API_STONE_SET_TEXT_LABEL, MACHINE_INFO_HARDWARE_VERSION);
}

static void app_hmi_get_running_status_(char * buff){

	if (true == app_coating_get_status_machine()){
		strcpy(buff, " Program Running");
	}

	else {
		strcpy(buff, "Idle");
	}

}

static void app_hmi_get_running_name_(char * buff){

	if (true == app_coating_get_status_machine()){
		strcpy(buff, "Running: ");
		app_coating_get_program_name(buff);

	}

	else {
		strcpy(buff, "Idle");
	}

}

static int64_t app_hmi_get_running_time_sec_(){

			if (true == app_coating_get_status_machine()){
				return  (board_timer_get_time_ms() - app_coating_get_last_start_time_program())/(int64_t)1000;
			}
			else {
				return 0;
			}
}

static float app_hmi_get_actual_temp_(void){
	return (float)api_bosch_bme280_get_temperature();
}

static float app_hmi_get_actual_press_(void){
	return (float) api_bosch_bme280_get_pressure();
}

static float app_hmi_get_actual_hum_(void){
	return (float) api_bosch_bme280_get_humidity();
}

static void app_hmi_save_bottom_limit_(float actual_limit){

	mod_coating_handlers_control_change_bottom_limit(actual_limit);

}
/********************** external functions definition ************************/

void app_hmi_goto_state_window_programs(void){
	rsc_statemachine_go_to(&self_.statemachine, state_window_programs_);
}

void app_hmi_goto_state_window_menu(void){
	rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
}

void app_hmi_goto_state_window_manual(void){
	rsc_statemachine_go_to(&self_.statemachine, state_window_manual_);
}

void app_hmi_goto_state_window_settings(void){
	rsc_statemachine_go_to(&self_.statemachine, state_window_settings_);
}

void app_hmi_init(void)
{
  xTaskCreate(app_hmi_loop, "app hmi task" 	// A name just for humans
  			, OS_CONFIG_APP_HMI_TASK_SIZE 			// This stack size can be checked & adjusted by reading the Stack Highwater
  			, NULL, OS_CONFIG_APP_HMI_TASK_PRIORITY 		// Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
  			, NULL);
}
void app_hmi_deinit(void){

}
void app_hmi_loop(void *pvParameters)
{

	widget_actions_config_();
	rsc_statemachine_init(&self_.statemachine, state_window_menu_);
	api_stone_open_window("menu");

  for(;;) {

	  //ESP_LOGI(TAG,__func__);
	  app_hmi_widget_parsing_();
	  rsc_statemachine_state(&self_.statemachine);


    vTaskDelay(OS_CONFIG_APP_HMI_TASK_PERIOD / portTICK_PERIOD_MS);

  }

}

/********************** Widgets handler funcs *******************/
//Tests
void widget_test_action_handler_( void){
	ESP_LOGI(TAG, "test widget ");
	api_stone_tx_set_text("nombre_wd_34", API_STONE_SET_TEXT_EDIT, "hola_hector", strlen("hola_hector"));
}

//Menu
void widget_menu_to_programs_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_programs_);

		//OPEN WINDOW
		api_stone_open_window("programs");

	}
}


void widget_menu_to_running_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_running_program_);

		//OPEN WINDOW
		api_stone_open_window("running_program");
	}
}

void widget_menu_to_new_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_new_program_);
		//OPEN WINDOW
		api_stone_open_window("new_program");
	}
}

void widget_menu_to_manual_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_manual_);
		//OPEN WINDOW
		api_stone_open_window("manual");
	}
}


void widget_menu_to_settings_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_settings_);
		//OPEN WINDOW
		api_stone_open_window("settings");
	}
}

//Programs
void widget_next_page_action_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_programs_vars_.next_page=true;
	}
}

void widget_prev_page_action_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_programs_vars_.previous_page=true;
	}
}

void widget_programs_to_menu_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
		//OPEN WINDOW
		api_stone_open_window("menu");
	}
}

void widget_slot1_selected_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_selected_program_);
		//OPEN WINDOW
		api_stone_open_window("selected_program");
		window_selected_program_vars_.triger_slotn=1;
	}
}

void widget_slot2_selected_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_selected_program_);
		//OPEN WINDOW
		api_stone_open_window("selected_program");
		window_selected_program_vars_.triger_slotn=2;
	}
}

void widget_slot3_selected_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_selected_program_);
		//OPEN WINDOW
		api_stone_open_window("selected_program");
		window_selected_program_vars_.triger_slotn=3;
	}
}

void widget_slot4_selected_handler_(void){

	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_selected_program_);
		//OPEN WINDOW
		api_stone_open_window("selected_program");
		window_selected_program_vars_.triger_slotn=4;
	}
}

void widget_slot5_selected_handler_(void){

}

void widget_programs_to_new_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_new_program_);
		//OPEN WINDOW
		api_stone_open_window("new_program");
	}
}

//Selected
void widget_play_selected_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_selected_program_vars_.play=true;
	}
}

void widget_delete_selected_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_selected_program_vars_.delete=true;
	}
}

void widget_selected_to_menu_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
		//OPEN WINDOW
		api_stone_open_window("menu");
	}
}

//Running
void widget_running_to_menu_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
		//OPEN WINDOW
		api_stone_open_window("menu");
	}
}

void widget_running_stop_handler_(void){
	ESP_LOGI(TAG,"%s",__func__);
	processCommand_t aux_process_comand;
	aux_process_comand.commandnumber = PROCESS_COMMAND_STOP;

	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		if (xQueueSend(xQueueIndividualCommand_Stop, &aux_process_comand, (TickType_t) 0) != pdPASS) {
				ESP_LOGE(TAG,"Command deleted, errQUEUE");
		}
	}
}

//New
void widget_new_to_menu_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
		//OPEN WINDOW
		api_stone_open_window("menu");
	}
}

void widget_new_play_handler_(void){

	ESP_LOGI(TAG,"%d",self_.wd_bt);

	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_new_program_vars_.play=true;
	}
}
void widget_new_save_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_new_program_vars_.save=true;
	}
}
void widget_new_name_handler_(void){
	ESP_LOGI(TAG,"%s: name %s",__func__,self_.wd_text);
	strcpy(window_new_program_vars_.program.name,self_.wd_text);
}
void widget_new_vel_down_handler_(void){
	ESP_LOGI(TAG,"%s -> value:%d",__func__,self_.wd_int);
	window_new_program_vars_.program.velocity_down=self_.wd_int;
}
void widget_new_accel_down_handler_(void){
	ESP_LOGI(TAG,"%s -> value:%d",__func__,self_.wd_int);
		window_new_program_vars_.program.acceleration_down=self_.wd_int;
}
void widget_new_wait_down_handler_(void){
	ESP_LOGI(TAG,"%s -> value:%d",__func__,self_.wd_int);
	window_new_program_vars_.program.wait_down=self_.wd_int;
}

void widget_new_vel_up_handler_(void){
	ESP_LOGI(TAG,"%s -> value:%d",__func__,self_.wd_int);
	window_new_program_vars_.program.velocity_up=self_.wd_int;
}
void widget_new_accel_up_handler_(void){
	ESP_LOGI(TAG,"%s -> value:%d",__func__,self_.wd_int);
	window_new_program_vars_.program.acceleration_up=self_.wd_int;
}
void widget_new_wait_up_handler_(void){
	ESP_LOGI(TAG,"%s -> value:%d",__func__,self_.wd_int);
	window_new_program_vars_.program.wait_up=self_.wd_int;
}
void widget_new_disp_to_sample_handler_(void){
	ESP_LOGI(TAG,"%s -> value:%d",__func__,self_.wd_int);
	window_new_program_vars_.program.displacement_to_sample=self_.wd_int;
}
void widget_new_to_pos_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s: value %d",__func__,self_.wd_int);
		rsc_statemachine_go_to(&self_.statemachine, state_window_position_);
		//OPEN WINDOW
		api_stone_open_window("position");
	}
}
void widget_new_depth_sample_handler_(void){
	ESP_LOGI(TAG,"%s -> value:%d",__func__,self_.wd_int);
	window_new_program_vars_.program.depth_sample=self_.wd_int;
}
void widget_new_loops_handler_(void){
	ESP_LOGI(TAG,"%s -> value:%d",__func__,self_.wd_int);
	window_new_program_vars_.program.loops=self_.wd_int;
}

//Info
void widget_info_to_settings_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_settings_);
		//OPEN WINDOW
		api_stone_open_window("settings");
	}
}

//Settings
void widget_settings_to_menu_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
		//OPEN WINDOW
		api_stone_open_window("menu");
	}
}

void widget_settings_reset_m_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		api_stone_open_window("home_page");
		app_hmi_reset_machine_();
	}
}

void widget_settings_origin_handler_(void){
	processCommand_t aux_process_comand;
	ESP_LOGI(TAG,"%s",__func__);
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		aux_process_comand.commandnumber = PROCESS_COMMAND_CERO_MACHINE;
		if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,(TickType_t ) 0) != pdPASS) {
			ESP_LOGE(TAG,"Command deleted, errQUEUE");
		}
	}
}

void widget_settings_bottom_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_bottom_limit_);
		//OPEN WINDOW
		api_stone_open_window("bottom_limit");
	}
}

void widget_settings_to_info_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_info_);
		//OPEN WINDOW
		api_stone_open_window("info");
	}
}

//Manual
void widget_manual_to_menu_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);

		//OPEN WINDOW
  	api_stone_open_window("menu");
	}
}

void widget_manual_dz1_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		api_stone_tx_set_visible("manual_dz1", true);
		api_stone_tx_set_visible("manual_dz2", false);
		api_stone_tx_set_visible("manual_dz3", false);
		window_manual_vars_.dz=1;

	}
}

void widget_manual_dz2_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		api_stone_tx_set_visible("manual_dz1", false);
		api_stone_tx_set_visible("manual_dz2", true);
		api_stone_tx_set_visible("manual_dz3", false);
		window_manual_vars_.dz=2;
	}
}

void widget_manual_dz3_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		api_stone_tx_set_visible("manual_dz1", false);
		api_stone_tx_set_visible("manual_dz2", false);
		api_stone_tx_set_visible("manual_dz3", true);
		window_manual_vars_.dz=3;
	}
}

void widget_manual_zero_handler_(void){
	processCommand_t aux_process_comand;
	ESP_LOGI(TAG,"%s",__func__);
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		aux_process_comand.commandnumber = PROCESS_COMMAND_CERO_MACHINE;
		if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,(TickType_t ) 0) != pdPASS) {
			ESP_LOGE(TAG,"Command deleted, errQUEUE");
		}
	}
}

void widget_manual_up_handler_(void){
	ESP_LOGI(TAG,"%s",__func__);
processCommand_t aux_process_comand;

if (1 == window_manual_vars_.dz){
	aux_process_comand.commandnumber = PROCESS_COMMAND_UPSLOW;
}
if (2 == window_manual_vars_.dz){
	aux_process_comand.commandnumber = PROCESS_COMMAND_UP;
}
if (3 == window_manual_vars_.dz){
	aux_process_comand.commandnumber = PROCESS_COMMAND_UPFAST;
}

	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
			if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,(TickType_t) 0) != pdPASS) {
				ESP_LOGE(TAG,"Command deleted, errQUEUE");
			}
	}
}

void widget_manual_down_handler_(void){
	ESP_LOGI(TAG,"%s",__func__);

	processCommand_t aux_process_comand;

	if (1 == window_manual_vars_.dz){
		aux_process_comand.commandnumber = PROCESS_COMMAND_DOWNSLOW;
	}
	if (2 == window_manual_vars_.dz){
		aux_process_comand.commandnumber = PROCESS_COMMAND_DOWN;
	}
	if (3 == window_manual_vars_.dz){
		aux_process_comand.commandnumber = PROCESS_COMMAND_DOWNFAST;
	}

	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
			if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,(TickType_t) 0) != pdPASS) {
				ESP_LOGE(TAG,"Command deleted, errQUEUE");
			}
	}

}

//Position
void widget_position_to_new_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_new_program_);
		//OPEN WINDOW
		api_stone_open_window("new_program");
		window_new_program_vars_.program.displacement_to_sample= api_tmc_get_actual_position();
		api_stone_tx_set_int("new_disp_sample", API_STONE_SET_TEXT_EDIT, window_new_program_vars_.program.displacement_to_sample);
	}
}


void widget_position_dz1_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		api_stone_tx_set_visible("position_dz1", true);
		api_stone_tx_set_visible("position_dz2", false);
		api_stone_tx_set_visible("position_dz3", false);
		window_position_vars_.dz=1;

	}
}

void widget_position_dz2_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		api_stone_tx_set_visible("position_dz1", false);
		api_stone_tx_set_visible("position_dz2", true);
		api_stone_tx_set_visible("position_dz3", false);
		window_position_vars_.dz=2;
	}
}

void widget_position_dz3_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		api_stone_tx_set_visible("position_dz1", false);
		api_stone_tx_set_visible("position_dz2", false);
		api_stone_tx_set_visible("position_dz3", true);
		window_position_vars_.dz=3;
	}
}

void widget_position_up_handler_(void){
	ESP_LOGI(TAG,"%s",__func__);
processCommand_t aux_process_comand;

if (1 == window_position_vars_.dz){
	aux_process_comand.commandnumber = PROCESS_COMMAND_UPSLOW;
}
if (2 == window_position_vars_.dz){
	aux_process_comand.commandnumber = PROCESS_COMMAND_UP;
}
if (3 == window_position_vars_.dz){
	aux_process_comand.commandnumber = PROCESS_COMMAND_UPFAST;
}

	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
			if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,(TickType_t) 0) != pdPASS) {
				ESP_LOGE(TAG,"Command deleted, errQUEUE");
			}
	}
}

void widget_position_down_handler_(void){
	ESP_LOGI(TAG,"%s",__func__);

	processCommand_t aux_process_comand;

	if (1 == window_position_vars_.dz){
		aux_process_comand.commandnumber = PROCESS_COMMAND_DOWNSLOW;
	}
	if (2 == window_position_vars_.dz){
		aux_process_comand.commandnumber = PROCESS_COMMAND_DOWN;
	}
	if (3 == window_position_vars_.dz){
		aux_process_comand.commandnumber = PROCESS_COMMAND_DOWNFAST;
	}

	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
			if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,(TickType_t) 0) != pdPASS) {
				ESP_LOGE(TAG,"Command deleted, errQUEUE");
			}
	}

}
//Bottom

void widget_bottom_limit_ok_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_bottom_vars_.actual_limit=api_tmc_get_actual_position();

		app_hmi_save_bottom_limit_(window_bottom_vars_.actual_limit);
		api_stone_tx_set_float("bottom_current_limit", API_STONE_SET_TEXT_LABEL, window_bottom_vars_.actual_limit);
	}
}

void widget_bottom_to_settings_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_settings_);
		//OPEN WINDOW
		api_stone_open_window("settings");
	}
}

void widget_bottom_dz1_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		api_stone_tx_set_visible("bottom_dz1", true);
		api_stone_tx_set_visible("bottom_dz2", false);
		api_stone_tx_set_visible("bottom_dz3", false);
		window_bottom_vars_.dz=1;

	}
}

void widget_bottom_dz2_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		api_stone_tx_set_visible("bottom_dz1", false);
		api_stone_tx_set_visible("bottom_dz2", true);
		api_stone_tx_set_visible("bottom_dz3", false);
		window_bottom_vars_.dz=2;
	}
}

void widget_bottom_dz3_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		api_stone_tx_set_visible("bottom_dz1", false);
		api_stone_tx_set_visible("bottom_dz2", false);
		api_stone_tx_set_visible("bottom_dz3", true);
		window_bottom_vars_.dz=3;
	}
}

void widget_bottom_up_handler_(void){
	ESP_LOGI(TAG,"%s",__func__);
processCommand_t aux_process_comand;

if (1 == window_bottom_vars_.dz){
	aux_process_comand.commandnumber = PROCESS_COMMAND_UPSLOW;
}
if (2 == window_bottom_vars_.dz){
	aux_process_comand.commandnumber = PROCESS_COMMAND_UP;
}
if (3 == window_bottom_vars_.dz){
	aux_process_comand.commandnumber = PROCESS_COMMAND_UPFAST;
}

	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
			if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,(TickType_t) 0) != pdPASS) {
				ESP_LOGE(TAG,"Command deleted, errQUEUE");
			}
	}
}

void widget_bottom_down_handler_(void){
	ESP_LOGI(TAG,"%s",__func__);

	processCommand_t aux_process_comand;

	if (1 == window_bottom_vars_.dz){
		aux_process_comand.commandnumber = PROCESS_COMMAND_DOWNSLOW;
	}
	if (2 == window_bottom_vars_.dz){
		aux_process_comand.commandnumber = PROCESS_COMMAND_DOWN;
	}
	if (3 == window_bottom_vars_.dz){
		aux_process_comand.commandnumber = PROCESS_COMMAND_DOWNFAST;
	}

	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
			if (xQueueSend(xQueueIndividualCommand, &aux_process_comand,(TickType_t) 0) != pdPASS) {
				ESP_LOGE(TAG,"Command deleted, errQUEUE");
			}
	}

}

void widget_bottom_default_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		window_bottom_vars_.actual_limit= MOD_COATING_HANDLERS_LOWER_LIMIT / MACHINE_STEPS_PER_MILLIMETER;

		app_hmi_save_bottom_limit_(window_bottom_vars_.actual_limit);
		api_stone_tx_set_float("bottom_current_limit", API_STONE_SET_TEXT_LABEL, window_bottom_vars_.actual_limit);
	}
}

//Popup saved

void widget_popupsaved_ok_handler_(void){
	if(API_STONE_BUTTON_RELEASE == self_.wd_bt){
		ESP_LOGI(TAG,"%s",__func__);
		rsc_statemachine_go_to(&self_.statemachine, state_window_menu_);
		api_stone_open_window("menu");
	}
}
/********************** internal statemachine ****** *******************/

static void state_window_menu_(rsc_statemachine_t *sm){

	static uint32_t start_time=0;
	static uint32_t actual_time=0;
	static char aux_string[APP_HMI_RUNNING_STATUS_STR_SIZE];

    //Entry
    if (rsc_statemachine_is_entry(sm))
    {

    	ESP_LOGI(TAG,"%s",__func__);
  		//OPEN WINDOW
  		api_stone_open_window("menu");

    	//START TIMERS
    	start_time = board_timer_get_time_ms();
    	actual_time = board_timer_get_time_ms();
    }
    //Loop

    //SEND REAL TIME DATA
  	if ( 500 < (actual_time - start_time)  ){

  		app_hmi_get_running_status_(aux_string);
  	  api_stone_tx_set_text("menu_status_running", API_STONE_SET_TEXT_LABEL, aux_string, strlen(aux_string));

  		start_time = board_timer_get_time_ms();
  	}

  	//UPDATE ACTUAL TIME
    actual_time = board_timer_get_time_ms();

    //Exit
    if (rsc_statemachine_is_exit(sm))
    {

    }
}
static void state_window_manual_(rsc_statemachine_t *sm){

		static uint32_t start_time=0;
		static uint32_t actual_time=0;
		//Entry
    if (rsc_statemachine_is_entry(sm))
    {

    	//OPEN WINDOW
    	api_stone_open_window("manual");
    	ESP_LOGI(TAG,"%s",__func__);

    	//START TIMERS
    	start_time = board_timer_get_time_ms();
    	actual_time = board_timer_get_time_ms();

			api_stone_tx_set_visible("manual_dz1", false);
			api_stone_tx_set_visible("manual_dz2", true);
			api_stone_tx_set_visible("manual_dz3", false);

			window_manual_vars_.dz=2;

    }
    //Loop

    //SEND REAL TIME DATA
#if 1

    if( false == app_coating_get_status_state_machine()){

  	if ( 500 < (actual_time - start_time)  ){
  		api_stone_tx_set_float("manual_pos", API_STONE_SET_TEXT_LABEL, api_tmc_get_actual_position());
  		  start_time = board_timer_get_time_ms();
  	}
    }

#endif
  	//UPDATE ACTUAL TIME
    actual_time = board_timer_get_time_ms();


    //Exit
    if (rsc_statemachine_is_exit(sm))
    {

    }
}
static void state_window_settings_(rsc_statemachine_t *sm){

    //Entry
    if (rsc_statemachine_is_entry(sm))
    {
    	//OPEN WINDOW
		api_stone_open_window("settings");
    	ESP_LOGI(TAG,"%s",__func__);
    }
    //Loop

    //Exit
    if (rsc_statemachine_is_exit(sm))
    {

    }
}

static void state_window_info_(rsc_statemachine_t *sm){

    //Entry
    if (rsc_statemachine_is_entry(sm))
    {
    	//OPEN WINDOW
		api_stone_open_window("info");
    	ESP_LOGI(TAG,"%s",__func__);

    	app_hmi_set_machine_info_();
    }
    //Loop



    //Exit
    if (rsc_statemachine_is_exit(sm))
    {

    }
}

static void state_window_programs_(rsc_statemachine_t* sm)
{
	static int32_t programs_in_mem = 0;
	static int32_t n_pages = 0;
	static api_program_manager_program_t aux_program;

    //Entry
    if (rsc_statemachine_is_entry(sm))
    {

    	//OPEN WINDOW
  		api_stone_open_window("programs");

    	ESP_LOGI(TAG,"%s",__func__);

    	//INIT VARS
    	window_programs_vars_.actual_page = 1;
    	window_programs_vars_.next_page = false;
    	window_programs_vars_.previous_page = false;

    	//interface with api programs
    	programs_in_mem = api_program_manager_get_n_programs_in_mem();
    	ESP_LOGI(TAG,"programs_in_mem: %d", programs_in_mem);
    	if(programs_in_mem <= APP_HMI_N_PROGRAMS_PER_PAGE){
    		n_pages = 1;
    	}
    	else{
    		n_pages = (int32_t)ceil((double)programs_in_mem/APP_HMI_N_PROGRAMS_PER_PAGE);
    	}
        	ESP_LOGI(TAG,"n_pages: %d", n_pages);


    	for (int i = 1; i <= APP_HMI_N_PROGRAMS_PER_PAGE; i++) {
    		if (true == api_program_manager_read(&aux_program, i)) {
    			ESP_LOGI(TAG,"%s",aux_program.name);
    			app_hmi_write_slot_name_(i,aux_program.name, strlen(aux_program.name));
    			ESP_LOGI(TAG,"READED SLOT %d: %s", i,  aux_program.name);
    		}
    		else{
    			app_hmi_write_slot_name_(i,"", strlen(""));
    			ESP_LOGI(TAG,"EMPTY SLOT %d", i);
    		}
    	}


     }
    // Loop

        if (true == window_programs_vars_.next_page){

        	window_programs_vars_.actual_page++;
        	ESP_LOGI(TAG,"actual_page: %d",window_programs_vars_.actual_page);

        	if(window_programs_vars_.actual_page <= n_pages ){
        		int k =1;
            	for (int i = (window_programs_vars_.actual_page-1)*APP_HMI_N_PROGRAMS_PER_PAGE+1;
            			i <= window_programs_vars_.actual_page*APP_HMI_N_PROGRAMS_PER_PAGE; i++) {

            		if (true == api_program_manager_read(&aux_program, i)) {
            			app_hmi_write_slot_name_(k,aux_program.name, strlen(aux_program.name));
            			ESP_LOGI(TAG,"READED SLOT %d: %s", i,  aux_program.name);
            		}
            		else{
            			app_hmi_write_slot_name_(k,"", strlen(""));
            			ESP_LOGI(TAG,"EMPTY SLOT %d", i);
            		}
            		k++;
            	}
        	}
        	else{
            	window_programs_vars_.actual_page = n_pages;
        		ESP_LOGI(TAG,"Last page");
        	}

            window_programs_vars_.next_page=false;
        }

       if (true == window_programs_vars_.previous_page){

    	   window_programs_vars_.actual_page--;

       	if(window_programs_vars_.actual_page  >= 1 ){
       		int k =1;
        	for (int i = (window_programs_vars_.actual_page-1)*APP_HMI_N_PROGRAMS_PER_PAGE+1;
        			i <= window_programs_vars_.actual_page*APP_HMI_N_PROGRAMS_PER_PAGE; i++) {
        		if (true == api_program_manager_read(&aux_program, i)) {
        			app_hmi_write_slot_name_(i,aux_program.name, strlen(aux_program.name));
        			ESP_LOGI(TAG,"READED SLOT %d: %s", i,  aux_program.name);
        		}
        		else{
        			app_hmi_write_slot_name_(i,"", strlen(""));
        			ESP_LOGI(TAG,"EMPTY SLOT %d", i);
        		}
        		k++;
        	}
       	}
       	else{
       		window_programs_vars_.actual_page = 1;
       		ESP_LOGI(TAG,"First page");
       	}
            window_programs_vars_.previous_page=false;
        }

    //Exit
    if (rsc_statemachine_is_exit(sm))
    {
    }
}

static void state_window_selected_program_(rsc_statemachine_t *sm){


	static api_program_manager_program_t aux_program;
    //Entry
    if (rsc_statemachine_is_entry(sm))
    {
    	//OPEN WINDOW
		api_stone_open_window("selected_program");
    	ESP_LOGI(TAG,"%s",__func__);
    	window_selected_program_vars_.selected=0;
    	window_selected_program_vars_.play=false;
    	window_selected_program_vars_.delete=false;
    }

    if(0!= window_selected_program_vars_.triger_slotn){

    	window_selected_program_vars_.selected= window_selected_program_vars_.triger_slotn + ((window_programs_vars_.actual_page-1) * APP_HMI_N_PROGRAMS_PER_PAGE);

		if (true == api_program_manager_read(&aux_program, window_selected_program_vars_.selected)) {
			app_hmi_write_selected_program_(aux_program);
			ESP_LOGI(TAG,"READED SLOT %d: %s", window_selected_program_vars_.selected,  aux_program.name);
		}
		else{
			ESP_LOGI(TAG,"EMPTY SLOT %d",window_selected_program_vars_.selected);
			rsc_statemachine_go_to(sm, state_window_programs_);

		}

		window_selected_program_vars_.triger_slotn=0;
    }

    if(0!= window_selected_program_vars_.selected){

    	//PLAY or DELETE
    	if(true == window_selected_program_vars_.play){

    		app_hmi_run_program_(aux_program);

    		rsc_statemachine_go_to(sm, state_window_running_program_);
    	}
    	else if(true == window_selected_program_vars_.delete){

    		app_hmi_delete_program_(window_selected_program_vars_.selected);

    		rsc_statemachine_go_to(sm, state_window_menu_);
    	}

    }
    //Exit
    if (rsc_statemachine_is_exit(sm))
    {
    	window_selected_program_vars_.triger_slotn=0;
    }
}

static void state_window_new_program_(rsc_statemachine_t *sm){

	static bool first_entry= true;

    //Entry
    if (rsc_statemachine_is_entry(sm))
    {
    	//OPEN WINDOW
		api_stone_open_window("new_program");
    	ESP_LOGI(TAG,"%s",__func__);

    	window_new_program_vars_.play=false;
    	window_new_program_vars_.save=false;

    	if(true == first_entry){
        	strcpy(window_new_program_vars_.program.name,"default_name");
        	window_new_program_vars_.program.velocity_down=1000;
        	window_new_program_vars_.program.acceleration_down=10000;
        	window_new_program_vars_.program.wait_down=1;

        	window_new_program_vars_.program.velocity_up=1000;
        	window_new_program_vars_.program.acceleration_up=10000;
        	window_new_program_vars_.program.wait_up=1;

        	window_new_program_vars_.program.depth_sample=20;
        	window_new_program_vars_.program.loops=1;

        	api_stone_tx_set_text("new_name", API_STONE_SET_TEXT_LABEL, window_new_program_vars_.program.name, strlen(window_new_program_vars_.program.name));
        	api_stone_tx_set_int("new_vel_down", API_STONE_SET_TEXT_EDIT, window_new_program_vars_.program.velocity_down);
        	api_stone_tx_set_int("new_accel_down", API_STONE_SET_TEXT_EDIT, window_new_program_vars_.program.acceleration_down);
        	api_stone_tx_set_int("new_wait_down", API_STONE_SET_TEXT_EDIT, window_new_program_vars_.program.wait_down);
        	api_stone_tx_set_int("new_vel_up", API_STONE_SET_TEXT_EDIT, window_new_program_vars_.program.velocity_up);
        	api_stone_tx_set_int("new_accel_up", API_STONE_SET_TEXT_EDIT, window_new_program_vars_.program.acceleration_up);
        	api_stone_tx_set_int("new_wait_up", API_STONE_SET_TEXT_EDIT, window_new_program_vars_.program.wait_up);
         	api_stone_tx_set_int("new_depth_sample", API_STONE_SET_TEXT_EDIT, window_new_program_vars_.program.depth_sample);
        	api_stone_tx_set_int("new_loops", API_STONE_SET_TEXT_EDIT, window_new_program_vars_.program.loops);

        	first_entry = false;
    	}

    	window_new_program_vars_.program.displacement_to_sample=api_tmc_get_actual_position();
     	api_stone_tx_set_int("new_disp_sample", API_STONE_SET_TEXT_EDIT, window_new_program_vars_.program.displacement_to_sample);
    }

	//PLAY or SAVE
	if(true == window_new_program_vars_.play){

		app_hmi_run_program_(window_new_program_vars_.program);

		rsc_statemachine_go_to(sm, state_window_running_program_);
	}
	else if(true == window_new_program_vars_.save){

		if(false == app_hmi_save_program_(&window_new_program_vars_.program)){
			api_stone_tx_set_text("popupsaved_label", API_STONE_SET_TEXT_LABEL, "ERROR: FULL MEMORY", strlen("ERROR: FULL MEMORY"));
		}
		else{
			api_stone_tx_set_text("popupsaved_label", API_STONE_SET_TEXT_LABEL, "SAVED", strlen("SAVED"));
			//rsc_statemachine_go_to(sm, state_window_menu_);
		}

		rsc_statemachine_go_to(sm, state_window_popup_saved_);


	}

    //Exit
    if (rsc_statemachine_is_exit(sm))
    {

    }
}

static void state_window_running_program_(rsc_statemachine_t *sm){

	static int64_t start_time=0;

	static char aux_string[APP_HMI_RUNNING_STATUS_STR_SIZE];

    //Entry
    if (rsc_statemachine_is_entry(sm))
    {
    	//OPEN WINDOW
    	api_stone_open_window("running_program");
    	ESP_LOGI(TAG,"%s",__func__);

    	//START TIMERS
    	start_time = board_timer_get_time_ms();


    }


    //SEND REAL TIME DATA
	if ( (board_timer_get_time_ms() - start_time) > 500 ){

		//RUNNING PROGRAM STATUS
		app_hmi_get_running_name_(aux_string);
  	  api_stone_tx_set_text("running_status_prog", API_STONE_SET_TEXT_LABEL, aux_string, strlen(aux_string));

  	  //RUNNING TIME
  	  api_stone_tx_set_int("running_time", API_STONE_SET_TEXT_LABEL, app_hmi_get_running_time_sec_());

#if 0
  	  //RUNNING POS
  	  api_stone_tx_set_int("running_pos", API_STONE_SET_TEXT_LABEL, api_tmc_get_actual_position());
#endif

	  //TEMP, PRESS & HUM
	  api_stone_tx_set_float("running_time_Sss_copy1", API_STONE_SET_TEXT_LABEL, app_hmi_get_actual_temp_());
	  //api_stone_tx_set_float("running_press", API_STONE_SET_TEXT_LABEL, app_hmi_get_actual_press_());
	  api_stone_tx_set_float("running_time_Sss", API_STONE_SET_TEXT_LABEL, app_hmi_get_actual_hum_());
		  //RESTART TIMER
		  start_time = board_timer_get_time_ms();
	}

    //Exit
    if (rsc_statemachine_is_exit(sm))
    {

    }

}

static void state_window_position_(rsc_statemachine_t *sm){

		static uint32_t start_time=0;
		static uint32_t actual_time=0;
		//Entry
    if (rsc_statemachine_is_entry(sm))
    {
		//OPEN WINDOW
		api_stone_open_window("position");
		ESP_LOGI(TAG,"%s",__func__);

    	//START TIMERS
    	start_time = board_timer_get_time_ms();
    	actual_time = board_timer_get_time_ms();

			api_stone_tx_set_visible("position_dz1", false);
			api_stone_tx_set_visible("position_dz2", true);
			api_stone_tx_set_visible("position_dz3", false);

			window_position_vars_.dz=2;

    }
    //Loop

    if( false == app_coating_get_status_state_machine()){
    //SEND REAL TIME DATA
  	if ( 500 < (actual_time - start_time)  ){
  	    api_stone_tx_set_float("position_pos", API_STONE_SET_TEXT_LABEL, api_tmc_get_actual_position());
  		  start_time = board_timer_get_time_ms();
  	}
    }

  	//UPDATE ACTUAL TIME
    actual_time = board_timer_get_time_ms();


    //Exit
    if (rsc_statemachine_is_exit(sm))
    {

    }
}

static void state_window_bottom_limit_(rsc_statemachine_t *sm){

		static uint32_t start_time=0;
		static bool first_entry = true;

		//Entry
	if (rsc_statemachine_is_entry(sm))
	{
		//OPEN WINDOW
		api_stone_open_window("bottom_limit");
		ESP_LOGI(TAG,"%s",__func__);


		//START TIMERS
		start_time = board_timer_get_time_ms();

		api_stone_tx_set_visible("bottom_dz1", false);
		api_stone_tx_set_visible("bottom_dz2", true);
		api_stone_tx_set_visible("bottom_dz3", false);
		window_bottom_vars_.dz=2;

		//BOTTOM LIMIT
		if(true == first_entry){
			window_bottom_vars_.actual_limit= mod_coating_handlers_control_get_bottom_limit();
			app_hmi_save_bottom_limit_(window_bottom_vars_.actual_limit);
			first_entry=false;
		}

		api_stone_tx_set_float("bottom_current_limit", API_STONE_SET_TEXT_LABEL, window_bottom_vars_.actual_limit);
		api_stone_tx_set_float("bottom_pos", API_STONE_SET_TEXT_LABEL, api_tmc_get_actual_position());
	}
	//Loop

  if( false == app_coating_get_status_state_machine()){
	//SEND REAL TIME DATA
		if ( 500 < (board_timer_get_time_ms() - start_time)  ){
			api_stone_tx_set_float("bottom_pos", API_STONE_SET_TEXT_LABEL, api_tmc_get_actual_position());
			start_time = board_timer_get_time_ms();
		}
  }

	//Exit
	if (rsc_statemachine_is_exit(sm))
	{

	}
}

static void state_window_popup_saved_(rsc_statemachine_t *sm){
	static uint32_t start_time=0;
	static bool first_entry = true;

	//Entry
	if (rsc_statemachine_is_entry(sm))
	{
		//OPEN WINDOW
		api_stone_open_window("popup_saved");
		ESP_LOGI(TAG,"%s",__func__);

	}
	//Loop


	//Exit
	if (rsc_statemachine_is_exit(sm))
	{

}
}


/********************** end of file ******************************************/
/** @}Final Doxygen */





