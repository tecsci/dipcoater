/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		app_test.c
* @date		14 jun. 2021
* @author	
*		-Martin Abel Gambarotta   magambarotta@gmail.com
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "app_test.h"
#include "api.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include <stdbool.h>
#include "os_config.h"

/********************** macros and definitions *******************************/

#define SERIAL_TEST 0
#define BLINK_TEST 0

#define TIEMPO_DESPLAZAMINETO_LINEAL_TEST 0
#define ACELERACIONs_TEST 0

/********************** internal data declaration ****************************/
static const char *TAG = "app_test";

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/
void app_test_init(void)
{
	xTaskCreate(app_test_loop, "test task" 	// A name just for humans
			, OS_CONFIG_APP_TEST_TASK_SIZE 			// This stack size can be checked & adjusted by reading the Stack Highwater
			, NULL, OS_CONFIG_APP_TEST_TASK_PRIORITY 		// Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
			, NULL);


}
void app_test_deinit(void){}
void app_test_loop(void *pvParameters)
{

 /* Inicializaciones */
 ESP_LOGI(TAG,"%s",__func__);

#if  TIEMPO_DESPLAZAMINETO_LINEAL_TEST

#endif

#if BLINK_TEST
  static bool state=true;
#endif

#if SERIAL_TEST
  uint8_t tx,rx;
  tx= 0x69;
  rx=0x03;
#endif


  for(;;) {

/*Ensayo para medir el tiempo total de dezplazamientos*/
#if  TIEMPO_DESPLAZAMINETO_LINEAL_TEST




#endif




/* TEST BLINK */
#if BLINK_TEST
      state=!state;
      api_gpio_write(API_GPIO_PIN_12, state);

#endif

/*TEST UART_1 */
#if SERIAL_TEST
      board_serial_write(API_UART_1,"HOLA HECTOR DESDE UART1\r\n");
      //api_serial_is_available(API_UART_1);
#endif

/*TEST SPI2*/
#if 0
      api_tmc_write_read(&tx,&rx,10);
#endif

      vTaskDelay(OS_CONFIG_APP_TEST_TASK_PERIOD/ portTICK_PERIOD_MS);
  }

}

/********************** end of file ******************************************/

/** @}Final Doxygen */
