idf_component_register(SRCS "app_console.c" "mod_console_commands.c"
                    INCLUDE_DIRS "include" "../api/include" "../api_stone/include" "../api_bosch/include" "../board/include" "../config/include" "../rsc/include" "../app_coating/include" "../tcm_helpers/include" "../tcm_ramp/include" "../tmc_comm/include" "../tmc_eval/include"
                    REQUIRES console)