/* 
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		api_stone_hmi.h
* @date		8 oct. 2021
* @author	
*		-Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef _API_STONE_HMI_H_
#define _API_STONE_HMI_H_
/******** inclusions ***************/

#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "stdint.h"
#include "string.h"

/******** macros *****************/

#define API_STONE_TX_MAX_DATA_LEN 100
#define API_STONE_TX_UART_NUM API_UART_1

/******** typedef ****************/
typedef enum{
	API_STONE_RX_CODE_BUTTON = 0x1001,
	API_STONE_RX_CODE_SWITCH = 0x1010,
	API_STONE_RX_CODE_CHECK  = 0x1020,
	API_STONE_RX_CODE_BUTTON_CS = 0x1002,
	API_STONE_RX_CODE_LABEL_TEXT = 0x1060,
	API_STONE_RX_CODE_LABEL_INT = 0x1062,
	API_STONE_RX_CODE_IMAGE = 0x1092,
	API_STONE_RX_CODE_EDIT_TEXT = 0x1070,
	API_STONE_RX_CODE_EDIT_INT = 0x1071,
	API_STONE_RX_CODE_EDIT_FLOAT = 0x1072
}api_stone_rx_code_t;


typedef enum{

	API_STONE_BUTTON_PRESSED = 0x01,
	API_STONE_BUTTON_RELEASE = 0x02,
	API_STONE_BUTTON_LONG = 0x03,

}api_stone_rx_button_val_t;

typedef enum{

	API_STONE_SWITCH_ENABLE = 0x01,
	API_STONE_SWITCH_DISABLE = 0x00,

}api_stone_rx_switch_val_t;



typedef enum{

	API_STONE_SET_TEXT_EDIT = 0,
	API_STONE_SET_TEXT_LABEL,
	API_STONE_SET_TEXT_SELECTOR,
	API_STONE_SET__N

}api_stone_tx_set_text_widget_type_t;



/******** external data declaration **********/

/******** external functions declaration *********/

bool api_stone_rx_process_button(char* name, api_stone_rx_button_val_t *p_button, uint8_t * data, uint32_t data_len);
bool api_stone_rx_process_switch(char* name, api_stone_rx_switch_val_t *p_button, uint8_t * data, uint32_t data_len);
bool api_stone_rx_process_edit_text(char* name, char* text, uint8_t * data, uint32_t data_len);
bool api_stone_rx_process_edit_int(char* name, int32_t* p_int, uint8_t * data, uint32_t data_len);

bool api_stone_tx_set_enabled(char * str_widget_name, bool enable );
bool api_stone_tx_set_visible(char * str_widget_name, bool visible );
bool api_stone_tx_set_text(char * str_widget_name, api_stone_tx_set_text_widget_type_t type, char* str_data, uint32_t data_len );
bool api_stone_tx_set_int(char * str_widget_name, api_stone_tx_set_text_widget_type_t type,  int32_t data );
bool api_stone_tx_set_float(char * str_widget_name, api_stone_tx_set_text_widget_type_t type,  float data );
bool api_stone_tx_set_switch_state(char * str_widget_name, bool enable);
bool api_stone_open_window(char * str_window_name );

void api_stone_hmi_init();
void api_stone_hmi_deinit();
void api_stone_hmi_loop();

#endif /* _API_STONE_HMI_H_ */
/** @}Final Doxygen */
