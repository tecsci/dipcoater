/* 
* <TECSCI Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		os_config.h
* @date		27 oct. 2021
* @author	
*	-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef _OS_CONFIG_H_
#define _OS_CONFIG_H_
/********************** TASKS CONFIG *******************************************/

//TASK PRIORITIES
#define OS_CONFIG_APP_TEST_TASK_PRIORITY 									(2)
#define OS_CONFIG_APP_HMI_TASK_PRIORITY 									(2)
#define OS_CONFIG_MOD_HMI_RX_TASK_PRIORITY 									(2)
#define OS_CONFIG_APP_CONSOLE_TASK_PRIORITY 								(2)

#define OS_CONFIG_APP_COATING_TASK_PRIORITY									(1)
#define OS_CONFIG_MOD_COATING_EMERGENCY_TASK_PRIORITY  						(1)

//TASKS LOOP PERIOD
#define OS_CONFIG_APP_TEST_TASK_PERIOD										(1000)
#define OS_CONFIG_APP_HMI_TASK_PERIOD 										(50)
#define OS_CONFIG_MOD_HMI_RX_TASK_PERIOD 									(10)

#define OS_CONFIG_APP_COATING_TASK_PERIOD 									(100)
#define OS_CONFIG_MOD_HANDLERS_COMMANDS_TASK_PERIOD							(100)
#define OS_CONFIG_MOD_HANDLERS_COMMANDS_TASK_PERIOD_LONG					(50)

//TASKS SIZES
#define OS_CONFIG_APP_TEST_TASK_SIZE										(4096)
#define OS_CONFIG_APP_HMI_TASK_SIZE 										(4096*2)
#define OS_CONFIG_MOD_HMI_RX_TASK_SIZE 						  				(4096)
#define OS_CONFIG_APP_CONSOLE_TASK_SIZE  					  				(4096*3)

#define OS_CONFIG_APP_COATING_TASK_SIZE 									(4096*2)
#define OS_CONFIG_MOD_COATING_EMERGENCY_TASK_SIZE   						(2048)


//QUEUE SIZE
#define OS_CONFIG_MOD_HMI_RX_QUEUE_SIZE 									(5)

/********************** SYS CONFIG PARAMS ***************************************/

#if 0
//SYS CONNECTION CONFIG
#define OS_CONFIG_WIFI_SSID ("Fibertel Heredia  2.4GHz")
#define OS_CONFIG_WIFI_PASSWORD ("0123456789")

//SYS ACCESSPOINT CONFIG
#define OS_CONFIG_AP_SSID "dip_ap"
#define OS_CONFIG_AP_PASS "012345678"

//SYS TCP CONFIG (in connection or accesspoint)
#define OS_CONFIG_TCP_ENABLE   			(true)

//SYS OTA UPDATE CONFIG
#define OS_CONFIG_SYSUPDATE_OTA_ENABLE   	(true)

#define OS_CONFIG_SYSUPDATE_OTA_PORT  (8266)
#define OS_CONFIG_SYSUPDATE_OTA_HOST ("myesp32")

#define OS_CONFIG_SYSUPDATE_SD_ENABLE 		(false) //DON'T ENABLE
#define OS_CONFIG_SYSUPDATE_SD_BINARY 		("/firmware.bin")

#endif

#endif /* _OS_CONFIG_H_ */
/** @}Final Doxygen */
