/* 
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2021>  
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		machine.h
* @date		15 jun. 2021
* @author	
*		-Martin Abel Gambarotta   (magambarotta@gmail.com)
* @version	v1.0.0
* 
* @brief
* @{ Init Doxygen
*/

#ifndef COMPONENTS_CONFIG_INCLUDE_MACHINE_H_
#define COMPONENTS_CONFIG_INCLUDE_MACHINE_H_
/********************** inclusions *******************************************/

/********************** macros ***********************************************/


/* INFO*/

#define MACHINE_INFO_ID 									"TECSCI_DIP_0002" //FAN MAYO 2023
#define MACHINE_INFO_FIRMWARE_VERSION 		1
#define MACHINE_INFO_HARDWARE_VERSION 		1


/* Configuración  velocidades[mm/min]  aceleracion  [m/min2] y distancia  [ mm]  desplazamientos manuales*/

#define  MACHINE_MANUAL_VELOCITY      		 (350)  //[mm/min]

#define  MACHINE_MANUAL_MEDIUM_ACCELERATION	(2000)  //[m/min2]
//#define  MACHINE_MANUAL_MAX_ACCELERATION	(2100)	//[m/min2]

#define  MACHINE_MANUAL_DISPLACEMENT_1		(1)			//[mm]
#define  MACHINE_MANUAL_DISPLACEMENT_10		(10)		//[mm]
#define  MACHINE_MANUAL_DISPLACEMENT_50		(50)		//[mm]


/*Buscar este numero con calibracion mecanica del sistema*/

//#define MACHINE_STEPS_PER_MILLIMETER							    (13057)		//(12932)
//#define MACHINE_STEPS_PER_MILLIMETER								(12916)		//(12932)
//#define MACHINE_STEPS_PER_MILLIMETER								(51200)
//Pasos version FAN Final
//#define MACHINE_STEPS_PER_MILLIMETER								(25600)


//Pasos version CNEA Final
#define MACHINE_STEPS_PER_MILLIMETER								(4667)

#define MACHINE_EXTERNAL_CLOCK										(16000000)	//16MHz


/*FACTORs*/

//#define MACHINE_USTEPS_VELOCITY_FACTOR						((MACHINE_EXTERNAL_CLOCK/2)*(1/8388608))	//	( fclock/2   *   1/2**23)
//#define MACHINE_USTEPS_VELOCITY_FACTOR						(0.9536743164)
#define MACHINE_USTEPS_VELOCITY_FACTOR 							(1)


//#define MACHINE_USTEPS_ACELERATION_FACTOR					((MACHINE_EXTERNAL_CLOCK*MACHINE_EXTERNAL_CLOCK) /  (512*256) / (16777216) ) //( fclock**2 / (512*256) /  2**24)
//#define MACHINE_USTEPS_ACELERATION_FACTOR						(116.4153218)
#define MACHINE_USTEPS_ACELERATION_FACTOR (116)

/*UPPER AND LOWER MECHANICAL LIMIT*/

#define MACHINE_CONTROL_MECANICAL_UPPER_LIMIT 		(MACHINE_STEPS_PER_MILLIMETER * 10 ) //	---	10mm
//#define MACHINE_CONTROL_MECANICAL_LOWER_LIMIT		(0x0024ED3E)   	// micro steps 4075910  --- 190mm
#define MACHINE_CONTROL_MECANICAL_LOWER_LIMIT		(MACHINE_STEPS_PER_MILLIMETER * 250) 		// micro steps 4075910  --- 290mm

#define MACHINE_CONTROL_MECANICAL_LOWER_LIMIT_STALLGUARD_DETECT  (2*MACHINE_CONTROL_MECANICAL_LOWER_LIMIT*(-1))
//#define MACHINE_CONTROL_MECANICAL_LOWER_LIMIT		(0x003E3186) 		// micro steps 4075910  --- 320mm


/*Desplazamiento relativo a parametros constructivos de la maquina*/

/*Conversion de Unidades

Configuracion de la maquina, motor paso a paso 1.8grados y 256 por paso
( 360/1.8 ) *256

51200 ustep por revolucion de motor

En esta maquina con este tornillo
MACHINE_STEPS_PER_MILLIMETER 							(12737)    //  12737 steps ==  1 mm

*/

/*Velocidad

1er conversion   a mm/s
V[mm/s] = V[mm/min]* 1/60 [min/s]

2er conversion a ustep/s
V[usteps/s]=V[mm/s] *   ( MACHINE_STEPS_PER_MILLIMETER [ustep/mm]  / 60 )

3er conversion por oscilador externo (pag.74 manual)  a registro TMC

V[para registro VMAX (0x27] = V[usteps/s] / ( fclock/2   *   1/2**23)
V[para registro VMAX (0x27] = V[usteps/s] / MACHINE_USTEPS_VELOCITY_FACTOR

 * */


//#define MACHINE_USTEPS_VELOCITY      ((MACHINE_STEPS_PER_MILLIMETER / 60)  *   1.050420168)
/*Uso este valor para evitar error de redondeo en las macros*/
//#define MACHINE_USTEPS_VELOCITY     (226.12) //(896.3585) //(226.12)

//Version FAN Final
//#define MACHINE_USTEPS_VELOCITY      448.17//75.3501 //((MACHINE_STEPS_PER_MILLIMETER / 60)  *   1.050420168)

//Version CNEA Final
#define MACHINE_USTEPS_VELOCITY      81 //81.67


/*Aceleracion
1er conversion   a mm/s
A[mm/s2] = A[m/min2]* 1/3600 [min2/s2] * 1000[mm/m]

2er conversion a ustep/s
A[usteps/s2]=A[mm/s2] * MACHINE_STEPS_PER_MILLIMETER [ustep/mm]

3er conversion por oscilador externo (pag.74 manual)

A[para registro AMAX-DMAX  (0x27 - 0x28)] = A[usteps/s2] / ( fclock**2 / (512*256) /  2**23)
A[para registro AMAX-DMAX (0x27 - 0x28)] = A[usteps/s] / MACHINE_USTEPS_ACELERATION_FACTOR
*/

//Global

//#define MACHINE_USTEPS_ACCELERATION 	( (MACHINE_STEPS_PER_MILLIMETER * 1000)  / (3600 * MACHINE_USTEPS_ACELERATION_FACTOR) ) //30.81877645
/*Uso este valor para evitar error de redondeo en las macros*/
//#define MACHINE_USTEPS_ACCELERATION   (30.81877645) //(122.1679) //(30.81877645)


//Version FAN Final
//#define MACHINE_USTEPS_ACCELERATION 		61.085//10.26//(MACHINE_STEPS_PER_MILLIMETER * 1000)/( 3600 * MACHINE_USTEPS_ACELERATION_FACTOR)

//Version CNEA Final
#define MACHINE_USTEPS_ACCELERATION 		11// 11.13


/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

#endif /* COMPONENTS_CONFIG_INCLUDE_MACHINE_H_ */
/** @}Final Doxygen */

